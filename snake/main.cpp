#include"snake.h"
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<fstream>
#include<conio.h>
using namespace ALL;
using namespace std;
const int App=10,Boom=20;
int n=30,m=35;
int mp[110][110],APPCNT,dirx,diry,Death;
const int Level[]={200,100,80,50,20,0};
const int M=1e5+10;
char *le[20]={"LV.1","LV.2","LV.3","LV.4","LV.5","Hard Demo"};
int level;
char name[12343];
char *load="Loading ... ... ... ...E";
void slowout(){
	for(int i=0;;i++){
		if(load[i]=='E') break;
		Sleep(60);
		putchar(load[i]);
	} putchar('\n');
}
void begin(){
	int now=0;
	puts("Input your Name:");
	scanf("%s",name);
	system("cls");
	while(1){
		setpos(hHome);
		setwhite();
		printf("Hi %s,Welcome to ",name);
		puts("Hungry Snake! Made By TimeTraveller!(If the screen is small,Please set at least 120x35)");
		puts("Select Level(W for up,S for Down,Enter for Yes):");
		for(int i=0;i<6;i++){
			if(now==i){
				setwater();
				puts(le[i]);
			}else{
				setblank();
				puts(le[i]);
			}
		}
		int tc=getch();
		if(tc=='w'){
			if(now>0)--now;
		}else if(tc=='s'){
			if(now<5)++now;
		}else if(tc==13){
			level=Level[now];break;
		}
	}
	system("cls");
	setblank();
	puts("Ins: ");
	puts("WASD for move!");
	setred();
	printf("��");
	setblank();
	printf(" is Apple\n");
	setyellow();
	printf("��");
	setblank();	
	printf(" is Boom\n");
	printf("Don't Eat yourself or the Wall!\n");
	slowout();
	printf("Press Any Key to Start!\n");
	getch();
}

struct point{
	int x,y;
	point(){}
	point(int a,int b):x(a),y(b){}
	bool operator ==(const point &a){
		return x==a.x&&y==a.y;
	}
};

struct Snake{
	point sk[M];
	int Len;
	void clear(){
		Len=0;memset(sk,0,sizeof(sk));
	}
}S,St;

bool check(Snake a,int x,int y){
	for(int i=1;i<=a.Len;i++){
		if(a.sk[i]==point(x,y)) return 0;
	}
	return 1;
}
bool flag;
int tcnt;
void spw_apple(){
	int tx=rand()%n+1,ty=rand()%m+1;
	while(!check(S,tx,ty)){
		tx=rand()%n+1,ty=rand()%m+1;
	}
	mp[tx][ty]=App;flag=1;
}
void Boom_spw(){
	int tx=rand()%n+1,ty=rand()%m+1;
	while(!check(S,tx,ty)||mp[tx][ty]){
		tx=rand()%n+1,ty=rand()%m+1;
	}
	mp[tx][ty]=Boom;
}
void Move(){
	if(isPressKey('W')){
		dirx=-1;diry=0;
	}else if(isPressKey('S')){
		dirx=1;diry=0;
	}else if(isPressKey('A')){
		dirx=0;diry=-1;
	}else if(isPressKey('D')){
		dirx=0;diry=1;
	}
	int tx=S.sk[1].x+dirx,ty=S.sk[1].y+diry;
	if(tx<=0||tx>n||ty<=0||ty>m) Death=1;
	else{
		if(mp[tx][ty]==Boom) Death=1;
		else for(int i=1;i<S.Len;i++){
			if(S.sk[i]==point(tx,ty)){
				Death=1;break;
			}
		}		
	}
	if(Death) return;
	St.clear();St.Len=S.Len;
	for(int i=2;i<=St.Len;i++){
		St.sk[i]=S.sk[i-1];
	}	St.sk[1]=point(tx,ty);
	S=St;
	if(mp[S.sk[1].x][S.sk[1].y]==App){
		flag=0;
		mp[S.sk[1].x][S.sk[1].y]=0;
		++APPCNT;++tcnt;
		if(tcnt>=10){
			tcnt=0;
			Boom_spw();
		}
		++S.Len;
		int xx=S.sk[S.Len-1].x-dirx,yy=S.sk[S.Len-1].y-diry;
		S.sk[S.Len]=point(xx,yy);
	}
}

void display(){
	setblank();
	printf("Len %5d | App %5d\n",S.Len,APPCNT);
	for(int i=1;i<=m;i++)printf("--");puts("--||");
	for(int i=1;i<=n;i++){
		printf("||");
		setblank();
		for(int j=1;j<=m;j++){
			if(!check(S,i,j)){
				if(point(i,j)==S.sk[1]){
					setwater();
					if(dirx==-1&&diry==0){
						printf("��");
					}else if(dirx==1&&diry==0){
						printf("��");
					}else if(dirx==0&&diry==1){
						printf("��");
					}else if(dirx==0&&diry==-1){
						printf("��");
					}
				}else{
					setgreen();
					printf("��");
				}
			}else if(!mp[i][j]){
				printf("  ");
			}else if(mp[i][j]==App){
				setred();
				printf("��"); 
			}else if(mp[i][j]==Boom){
				setyellow();
				printf("��");
			}
		}
		setblank();
		printf("||\n");
	}
	setblank();
	for(int i=1;i<=m;i++)printf("--");
	puts("--||");
}

void Work(){
	begin();
	S.Len=1;S.sk[1].x=5;S.sk[1].y=5;
	dirx=1;diry=0;
	Boom_spw();
	while(1){
		setpos(hHome);
		if(!flag) spw_apple();
		display();
		Move();
		if(level)Sleep(level);
		if(Death) break;
	}
	setpos(hHome);
	display();
	printf("You Died!");
	Sleep(3000);
	system("cls");
	setblank();
	printf("Your Len is %5d,Eat %5d Apple\n",S.Len,APPCNT);
	printf("Press AnyKey to End!");
	ofstream fi;
	fi.open("sc.log",std::ios::app|std::ios::out);
	fi<<name<<": Len Growed "<<S.Len<<" || Apple Eated "<<APPCNT<<"\n";
	getch();getch();
}

int main(){HideCursor();Work();}
