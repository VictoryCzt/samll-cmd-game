#ifndef _BANK_SYSTEM_
#define _BANK_SYSTEM_
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<windows.h>
#include<fstream>
#include<string>
#include<conio.h>
#define ll long long
using namespace std;
const int M=10001;
bool isPressKey(int a){
	return GetAsyncKeyState(a)?1:0;
}
void clearout(){
	for(int i=1;i<=100;i++){
		isPressKey('A');
	}
}
struct user{
	char user_name[40];
	char password[50];
	int use,pass;
	ll money;
	user(){use=pass=money=0;}
	user(char *a,char *b){
		use=strlen(a);pass=strlen(b);
		for(int i=0;i<use;i++)user_name[i]=a[i];user_name[use]=0;
		for(int i=0;i<pass;i++)password[i]=b[i];password[pass]=0;
		money=0;
	}
}u[M];
char suser[100],spass[100];
int tot;
void init(){
	ifstream op;op.open("userdata.info");
	op>>tot;
	for(int i=1;i<=tot;i++){
		op>>u[i].user_name>>u[i].password>>u[i].use>>u[i].pass>>u[i].money;
	}
	op.close();
}
int pos;
void register_user(){
	be:
	system("cls");
	printf("请输入用户名(不超过20字):");
	scanf("%s",suser);
	for(int i=1;i<=tot;i++){
		if(strcmp(u[i].user_name,suser)==0){
			printf("用户名重复!!!\n");
			Sleep(3000);
			goto be;
		}
	}
	printf("输入密码(只包含字母与数字，不超过50字):");
	int cnt=0;
	for(;cnt<=40;cnt++){
		spass[cnt]=getch();
		if(spass[cnt]=='\r') break;
		printf("*");
	}	spass[cnt]=0;
	puts("");
	u[++tot]=user(suser,spass);
	printf("注册成功!!!\n请妥善保管你的用户名和密码\n");
	pos=tot;
//	printf("%d\n",pos);
	Sleep(5000);
	system("cls");
	return;
}
void login(){
	be:
	system("cls");
	printf("输入用户名:");
	scanf("%s",suser);pos=0;
	for(int i=1;i<=tot;i++){
		if(strcmp(u[i].user_name,suser)==0){pos=i;break;}
	}
	if(!pos){printf("无此用户!\n");goto be;}
	printf("输入密码:");
	int cnt=0;
	for(;cnt<=40;cnt++){
		spass[cnt]=getch();
		if(spass[cnt]=='\r') break;
		printf("*");
	}	spass[cnt]=0;
	if(strcmp(u[pos].password,spass)){
		printf("\n密码错误!!\n");
		Sleep(1500);
		goto be;
	}
	puts("");
	printf("登入成功，欢迎%s。\n",suser);
	Sleep(5000);
	system("cls");
}
ll in,out;
void savein(){
	system("cls");
	printf("输入存入金额:");
	scanf("%lld",&in);
	u[pos].money+=in;
	printf("成功存入%lld￥,现在账户余额:%lld￥\n",in,u[pos].money);
//	printf("%d\n",pos);
	Sleep(5000);
	system("cls");
}
void takeout(){
	be:
	system("cls");
	printf("输入取出金额:");
	scanf("%lld",&out);
	if(u[pos].money-out<0){
		printf("账户余额不足，您的账户余额为%lld￥\n",u[pos].money);
		Sleep(3000);
		goto be;
	}
	u[pos].money-=out;
	printf("成功取出%lld￥,您的账户余额为%lld￥\n",out,u[pos].money);
	Sleep(5000);
	system("cls");
}
void save(){
	freopen("userdata.info","w",stdout);
	printf("%d\n",tot);
	for(int i=1;i<=tot;i++){
		printf("%s %s %d %d %lld\n",u[i].user_name,u[i].password,u[i].use,u[i].pass,u[i].money);
//		in<<u[i].user_name<<" "<<u[i].password<<" "<<u[i].use
//		<<" "<<u[i].pass<<" "<<u[i].money<<endl;
	}
//	in.close();
}
void trans(){
	be:
	system("cls");
	printf("输入交易的账户名和交易金额:");
	scanf("%s%lld",suser,&in);int top=0;
	if(u[pos].money-in<0){
		printf("您的余额不足，您的余额为%lld￥\n",u[pos].money);
		Sleep(4000);
		goto be;
	}
	if(!strcmp(suser,u[pos].user_name)){
		printf("不能转给自己!!\n");
		Sleep(4000);
		goto be;
	}
	for(;top<=tot;top++){
		if(top==pos) continue;
		if(!strcmp(u[top].user_name,suser)){
			break;
		}
	}
	if(!top){
		printf("无此用户!!\n");
		Sleep(5000);
		goto be;
	}
	u[top].money+=in;
	u[pos].money-=in;
	printf("交易成功，您的余额为%lld￥\n",u[pos].money);
	Sleep(5000);
	system("cls");
}
void check(){
	system("cls");
	clearout();
	printf("您的余额为%lld￥\n",u[pos].money);
	Sleep(5000);
	system("cls");
}
char cao[43];
int main(){
	init();
	printf("欢迎来到私人银行终端系统!\n");
	Sleep(2000);
	printf("按L键登陆，按R键注册新用户\n");
	while(1){
		if(isPressKey('L')){
			getch();
			login();
			break;
		}
		if(isPressKey('R')){
			getch();
			register_user();
			break;
		}
	}
	clearout();
	system("cls");
	while(1){
		printf("请输入你要执行的交易:\n");
		printf("ck 查看余额\ntr 转账\nin 存入\nout 取出\nok 结束\n");
		scanf("%s",cao);
		if(!strcmp(cao,"ck")){
			check();
		}else if(!strcmp(cao,"tr")){
			trans();
		}else if(!strcmp(cao,"in")){
			savein();
		}else if(!strcmp(cao,"out")){
			takeout();
		}else if(!strcmp(cao,"ok")){
			break;
		}else{
			printf("无效操作!\n");
			Sleep(3000);
			system("cls");
		}
	}
	printf("%s,谢谢使用本银行自助终端!\n",u[pos].user_name);
	save();
	Sleep(5000);
	return 0;
}
#endif
