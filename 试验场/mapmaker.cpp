#include<cstdio>
#include<cstring>
#include<algorithm>
#include<scrlib.h>
#include<fstream>
using namespace ALL;
using namespace std;
const int M=1000;
int mp[M][M];

void slowout(char *s,int n){
	for(int i=0;i<=n;i++){
		Sleep(50);
		printf("%c",s[i]);
	}
}
void out(){
	setpos(hHome);
	printf("左键绘制,右键del(坐标均大于0小于200).Q:墙W:怪物出生点B:玩家出生点(有且只有一个),按L结束绘图");
}
bool check(COORD a){
	if(a.X>0&&a.X<=200&&a.Y>0&&a.Y<=200) return 1;
	return 0;
}
int maxx,maxy;
int id[223];
char c='Q',file[213];
void check2(){
	if(isPressKey('Q')) c='Q';
	else if(isPressKey('W')) c='W';
	else if(isPressKey('B')) c='B';
}
bool flag;int cnt=0;
int main(void)
{
	id['Q']=1;id['W']=20;id['B']=2;
    // 获取标准输入输出设备句柄
    HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
    HANDLE hIn = GetStdHandle(STD_INPUT_HANDLE);

    CONSOLE_SCREEN_BUFFER_INFO bInfo;
    INPUT_RECORD    mouseRec;
    DWORD           res;
    COORD           crPos, crHome = {0, 0};

//    printf("[Cursor Position] X: %2lu  Y: %2lu\n", 0, 0);   // 初始状态

    while (1)
    {
    	check2();
    	out();
        ReadConsoleInput(hIn, &mouseRec, 1, &res);

        if (mouseRec.EventType == MOUSE_EVENT)
        {
            if (mouseRec.Event.MouseEvent.dwButtonState==FROM_LEFT_1ST_BUTTON_PRESSED)
            {
                if (mouseRec.Event.MouseEvent.dwEventFlags == DOUBLE_CLICK)
                {
                    break;  // 左键双击 退出循环
                }
            }

            crPos = mouseRec.Event.MouseEvent.dwMousePosition;
            GetConsoleScreenBufferInfo(hOut, &bInfo);
//            SetConsoleCursorPosition(hOut, crHome);
            printf("X: %2lu  Y: %2lu    ", crPos.X, crPos.Y);
            SetConsoleCursorPosition(hOut, bInfo.dwCursorPosition);

            switch (mouseRec.Event.MouseEvent.dwButtonState)
            {
            case FROM_LEFT_1ST_BUTTON_PRESSED:          // 左键 输出A
                
                if(check(crPos)){
                	if(c=='B'&&flag){
                		if(cnt<1)
                		MessageBox(NULL,"已经有一个出生点","警告",MB_OK),cnt++;
					}else{
					FillConsoleOutputCharacter(hOut, c, 1, crPos, &res);
                	mp[crPos.Y][crPos.X]=id[c];
					if(c=='B'){flag=1;cnt=0;}
                	maxx=max(maxx,(int)crPos.Y);maxy=max(maxy,(int)crPos.X);
					}
				}
                break;

            case RIGHTMOST_BUTTON_PRESSED:              // 右键 输出a
                FillConsoleOutputCharacter(hOut, ' ', 1, crPos, &res);
                if(check(crPos)){
                	if(mp[crPos.Y][crPos.X]==2) flag=0;
                	mp[crPos.Y][crPos.X]=0;
				}
                break;

            default:
                break;
            }
        }
        if(isPressKey('L')){
        	break;
		}
    }
    clearout();
    slowout("End! Your size is",16);printf(" %d %d\n",maxy,maxx);
    system("pause");
    clearout();
	int n,m,tot,bos,kl,level;
	printf("请输入地图长宽，和敌人总数和每波敌人数量(要保证总数为每波敌人数量的倍数)\n和敌人难度(1~100越高越难)还有第几个关卡\n");
	scanf("%d%d%d%d%d%d",&n,&m,&tot,&bos,&kl,&level);
	sprintf(file,"map%d.date",level);
	ofstream ut;
	ut.open(file);
	ut<<m<<" "<<n<<" "<<tot<<" "<<bos<<" "<<kl<<endl;
	for(int i=1;i<=m;i++){
		for(int j=1;j<=n;j++){
			ut<<mp[i][j]<<" ";
		}   ut<<endl;
	}
	ut.close();
    CloseHandle(hOut);  // 关闭标准输出设备句柄
    CloseHandle(hIn);   // 关闭标准输入设备句柄
    return 0;
}
