#include<cstdio>
#include<cstring>
#include<algorithm>
#include<scrlib.h>
#include<iostream>
using namespace std;
using namespace ALL;
const int M=504;
char map[M][M];
int tx,ty,ammo,hp,ar;
struct bt{
	int dx,dy;
	bt(int a=0,int b=0):dx(a),dy(b){}
}bb[M][M];
void check(int a,int b,int c,int d){
	
}
void out(int a,int b,int c,int d){
	if(b-a+1<20){
		if(a==0) b=20;
		else if(b==299) a=279;
	}
	for(int i=a;i<=b;i++){
		for(int j=c;j<=d;j++){
			if(i==tx&&j==ty){
				setblue();
				printf("Q");
				continue;
			}
			if(map[i][j]=='1'){
				setgreen();
				printf("Y");
			}else if(map[i][j]=='2'){
				setyellow();
				printf("O");
			}else if(map[i][j]=='3'){
				setwater();
				printf("O");
			}else if(map[i][j]=='4'){
				setwhite();
				printf("#");
			}else if(map[i][j]=='5'){
				setbackred();
				printf("X");
			}else{
			setblank();
			printf(" ");
			} 
		}
		printf("\n");
	}
	setblank();
	printf("Ammo: %d | HP: %d | Arom: %d\n",ammo,hp,ar);
}
void outmap(){
	out(max(0,tx-10),min(299,tx+10),max(0,ty-25),min(299,ty+25));
}
void move(){
	if(isPressKey('W')){
		tx=max(0,tx-1);
	}
	if(isPressKey('A')){
		ty=max(0,ty-1);
	}
	if(isPressKey('D')){
		ty=min(299,ty+1);
	}
	if(isPressKey('S')){
		tx=min(299,tx+1);
	}
}
#include<fstream>
int main(){
	tx=50,ty=50;
	ifstream mapdate("map.in");
	for(int i=0;i<300;i++){
		for(int j=0;j<300;j++){
			mapdate>>map[i][j];
		}
	}
	while(1){
		setpos(hHome);
		outmap();
		move();
	}
}

