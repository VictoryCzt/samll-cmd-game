#include<cstdio>
#include<cstring>
#include<algorithm>
#include"scrlib.h"
#include<fstream>
using namespace ALL;
using namespace std;
const int M=200;
int mp[M][M],n,m,ene,bo,ad;
char map[20],name[20];
const char mode[][20]={"","Easy","Normal","Hard"};
int dx[]={0,1,0,-1,1,1,-1,-1},dy[]={1,0,-1,0,1,-1,1,-1};
int HP,AMMO,DA,kill,tx,ty,dr;
struct bullet{
	int da,d,t;
	bool flag;
	void clear(){
		da=d=flag=0;
	}
}bu[M][M];
struct mon{
	int hp,da,t;
	void clear(){
		hp=da=t=0;
	}
}mo[M][M];
int bohp[M][M];
void clearoutt(){for(int i=1;i<=200;i++)printf("  ");}
void slowout(char *s,int n){
	for(int i=0;i<=n;i++){
		Sleep(50);
		printf("%c",s[i]);
	}
}
int sele(int now){
	Sleep(150);
	if(isPressKey('W')) {now--;if(now<=0) now=1;}
	if(isPressKey('S')) {now++;if(now>3) now=3;}
	return now;
}
void stop(){
	if(isPressKey('M')){
		while(1){
			if(isPressKey('M')) break;
		}
	}
}
void out(int a,int b,int c,int d){
	if(a<=0) a=1;if(b>n) b=n;
	if(c<=0) c=1;if(d>m) d=m;
	if(b-a+1<20){
		if(a==1) b=20;
		else if(b==n) a=max(n-20,1);
	}
	if(d-c+1<20){
		if(c==1) d=20;
		else if(d==m) c=max(m-20,1);
	}
	clearoutt();
//	printf("%d %d %d %d| %d %d \n",a,b,c,d,tx,ty);
	for(int i=a;i<=b;i++){
		for(int j=c;j<=d;j++){
			if(tx==i&&ty==j) {setwater();
			if(dr==1) printf("^");
			else if(dr==2) printf(">");
			else if(dr==4) printf("<");
			else if(dr==3) printf("V");
			continue;
			}
			if(mo[i][j].hp>0){
				setpink();printf("O");continue;
			}
			if(bohp[i][j]){setblue();printf("@");continue;}
			if(bu[i][j].flag){setyellow();printf("*");continue;}
			if(mp[i][j]==1){setwhite();printf("#");}
			else if(mp[i][j]==4){setgreen();printf("A");}
			else if(mp[i][j]==5){setred();printf("+");}
			else if(mp[i][j]==6){setred();printf("*");}
			else printf(" ");
		}
		printf("\n");
	}
	setblank();
	printf("HP: %4d          \nDA: %4d          \nAMMO: %4d       \nKill: %4d         \n",HP,DA,AMMO,kill);
	printf("Press M to stop or continue!\n");

}
int outbegin(){
	slowout("Welcome ",7);
	slowout(name,strlen(name)-1);
	slowout(" to the Battle!",14);
	Sleep(500);
	setpos(hHome);clearoutt();setpos(hHome);
	printf("WASD for move,H key for gun,J key for knife;\nAnd the A to add AMMO,+ to add HP,The target is to kill all the monster and survive!\n");
	printf("If you clear,Press K to continue!\n");
	while(1){
		if(isPressKey('K')){
			clearout();
			break;
		}
	}
	int tpos=1;
	Hide();
	while(1){
		setpos(hHome);
		printf("Select Level:(WSF:up,down,ok)\n");
		tpos=sele(tpos);
		for(int i=1;i<=3;i++){
			if(tpos==i) setbackwhite();
			printf("%s\n",mode[i]);
			setblank();
		}
		if(isPressKey('F')) {break;}
	}
	return tpos;
}
int id[M];
char nam[M][20];
int sc[M];
bool cmp(int a,int b){
	return sc[a]>sc[b];
}
void outend(){
	clearout();
	if(HP<=0)slowout("You Have Die!",12);
	else slowout("You Win!",7);
	puts("");
	slowout(name,strlen(name)-1);printf("\nYou Have Kill %d Monster!\n",kill);
	ifstream list("list.date");
	int tot;
	list>>tot;
	for(int i=1;i<=tot;i++){
		id[i]=i;
		list>>nam[i]>>sc[i];
		printf("%s Kill:%d\n",nam[i],sc[i]);
	}
	++tot;id[tot]=tot;
	for(int i=0;name[i];i++){
		nam[tot][i]=name[i];
	}
	sc[tot]=kill;
	ofstream listp("list.date");
	sort(id+1,id+tot+1,cmp);
	listp<<tot<<endl;
	for(int i=1;i<=tot;i++){
		listp<<nam[i]<<" "<<sc[i]<<endl;
	}
	list.close();
	listp.close();
}
void outpass(int i,int j){
	clearout();
	printf("You Have Pass %d\%d level\n",i,j);
	Sleep(1500);
	clearout();
}
struct line{
	int x,y;
	line(int a=0,int b=0):x(a),y(b){}
	line operator -(line a){
		return line(x-a.x,y-a.y);
	}
}l(1,1),r(-1,1),o;
int dot(line a,line b){
	return a.x*b.x+a.y*b.y;
}
//POINT tt;
void checkdr(){
//	GetCursorPos(&tt);
//	o=line(tt.x-tx,tt.y-ty);
//	printf("MOUSE %d %d\n",tt.x,tt.y);
//	line x1=l-line(tx,ty),x2=r-line(tx,ty);
//	int t1=dot(l,o),t2=dot(r,o);
//	if(t1>=0&&t2>=0){
//		dr=2;
//	}else if(t1>=0&&t2<0){
//		dr=1;
//	}else if(t1<0&&t2<0){
//		dr=3;
//	}else if(t1<0&&t2>=0){
//		dr=4;
//	}
}
int tttt;line ex[M];
bool ot(int a,int b){
	if(a<=0||a>n||b<=0||b>m) return 1;return 0;
}
int last,tlast;
bool randene(){
	bool tpp=0;
	int tp=rand()%tttt;
	int xx=ex[tp].x,yy=ex[tp].y;
	for(int i=0;i<8;i++){
		int ttx=xx+dx[i],tty=yy+dy[i];
		if(mp[ttx][tty]==1||ot(ttx,tty)) continue;
		if(!mo[ttx][tty].hp){
			mo[ttx][tty].hp=15+ad;
			mo[ttx][tty].da=5+ad;
			mo[ttx][tty].t=last+1;
			tpp=1;
			break;
		}
	}
	return tpp;
}
int kl;
void check(int tim,bool flag){
	if(tim%4==0){
	last++;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			if(mo[i][j].hp>0&&mo[i][j].t==last){
				int dxx=i,dyy=j;
				if(dxx==tx&&dyy==ty) {
					HP-=mo[i][j].da;
					mo[i][j].t=last+1;
					continue;
				}
				int now=rand()%100;
				if(now<=kl){
				if(dxx>tx-1) dxx--;
				else if(dxx<tx-1) dxx++;
				if(dyy>ty-1) dyy--;
				else if(dyy<ty-1) dyy++;
				}else{
					int tot=1000;
					while(tot--){
						now=rand()%7;
						int wx=dxx+dx[now],wy=dyy+dy[now];
						if(ot(wx,wy)) continue;
						if(mo[wx][wy].hp||mp[wx][wy]==1) continue;
						else {
							dxx=wx;dyy=wy;
							break;
						}
					}
				}
				if(mo[dxx][dyy].hp>0||ot(dxx,dyy)){
					mo[i][j].t=last+1;
					continue;
				}
				mo[dxx][dyy].hp=mo[i][j].hp;
				mo[dxx][dyy].t=last+1;
				mo[dxx][dyy].da=mo[i][j].da;
				mo[i][j].clear();
			}
		}
	}
	}
		tlast++;
		for(int i=1;i<=n;i++){
			for(int j=1;j<=m;j++){
				if(bu[i][j].flag&&bu[i][j].t==tlast){
				if(mp[i][j]==1) {
					bu[i][j].clear();continue;
				}
					if(mo[i][j].hp>0){
						mo[i][j].hp-=bu[i][j].da;
						if(mo[i][j].hp<=0) mo[i][j].clear(),kill++;
						bu[i][j].clear();
						continue;
					}
					int xx=i,yy=j;
					xx+=dx[bu[i][j].d];yy+=dy[bu[i][j].d];
					if(ot(xx,yy)){
						bu[i][j].clear();continue;
					}
					bu[xx][yy].flag=1;
					bu[xx][yy].da=bu[i][j].da;
					bu[xx][yy].d=bu[i][j].d;
					bu[xx][yy].t=tlast+1;
					bu[i][j].clear();
				}
			}
		}
}
const int idi[][4]={{0,0,0,0},{0,3,6,7},{0,0,6,4},{0,1,4,5},{0,2,7,5}};
void fire(){
	if(AMMO<=0) return;
	bool f=0;
	for(int i=1;i<=3;i++){
		int xx=tx+dx[idi[dr][i]],yy=ty+dy[idi[dr][i]];
		if(ot(xx,yy)) continue;
		f=1;
		bu[xx][yy].da=DA+10;
		bu[xx][yy].d=idi[dr][i];
		bu[xx][yy].t=tlast+1;
		bu[xx][yy].flag=1;
	}
	AMMO-=f;
}
void knife(bool ok){
	for(int i=0;i<8;i++){
		int ss=tx+dx[i],sss=ty+dy[i];
		if(ot(ss,sss)) continue;
		if(!mp[ss][sss])mp[ss][sss]=6;
	}
	COORD t11={0,ok};
	setpos(t11);
	out(tx-20,tx+20,ty-20,ty+20);
	setpos(hHome);
	for(int i=0;i<8;i++){
		int ss=tx+dx[i],sss=ty+dy[i];
		if(ot(ss,sss)) continue;
		if(mp[ss][sss]==6)mp[ss][sss]=0;
	}
	if(dr==1){
		int t1=tx-1;
		for(int i=ty-1;i<=ty+1;i++){
			if(ot(t1,i)) continue;
			if(mo[t1][i].hp>0){
				mo[t1][i].hp-=DA+5;
				if(mo[t1][i].hp<0) mo[t1][i].clear(),kill++;
			}
		}
	}else if(dr==2){
		int t1=ty+1;
		for(int i=tx-1;i<=tx+1;i++){
			if(ot(t1,i)) continue;
			if(mo[t1][i].hp>0){
				mo[t1][i].hp-=DA+5;
				if(mo[t1][i].hp<0) mo[t1][i].clear(),kill++;
			}
		}
	}else if(dr==3){
		int t1=tx+1;
		for(int i=ty-1;i<=ty+1;i++){
			if(ot(t1,i)) continue;
			if(mo[t1][i].hp>0){
				mo[t1][i].hp-=DA+5;
				if(mo[t1][i].hp<0) mo[t1][i].clear(),kill++;
			}
		}
	}else if(dr==4){
		int t1=ty-1;
		for(int i=tx-1;i<=tx+1;i++){
			if(ot(t1,i)) continue;
			if(mo[t1][i].hp>0){
				mo[t1][i].hp-=DA+5;
				if(mo[t1][i].hp<0) mo[t1][i].clear(),kill++;
			}
		}
	}
}
void move(bool ok){
	Sleep(20);
	int nx=tx,ny=ty;
	if(isPressKey('W')){
		tx--;if(tx<=0) tx=1;
		dr=1;
	}
	if(isPressKey('S')){
		tx++;if(tx>n) tx=n;
		dr=3;
	}
	if(isPressKey('A')){
		ty--;if(ty<=0) ty=1;
		dr=4;
	}
	if(isPressKey('D')){
		ty++;if(ty>m) ty=m;
		dr=2;
	}
	if(mp[tx][ty]==1) tx=nx,ty=ny;
	if(mp[tx][ty]==5){
		mp[tx][ty]=0;
		HP+=20;HP=min(HP,100);
	}else if(mp[tx][ty]==4){
		mp[tx][ty]=0;
		AMMO+=100;
	}
	if(isPressKey('H')){
		fire();
	}
	if(isPressKey('J')){
		knife(ok);
	}
	checkdr();
}
void randThing(){
	while(1){
		int x=rand()%n+1,y=rand()%m+1;
		if(!mp[x][y]&&!ot(x,y)){
			mp[x][y]=4;break;
		}
	}
	while(1){
		int x=rand()%n+1,y=rand()%m+1;
		if(!mp[x][y]&&!ot(x,y)){
			mp[x][y]=5;break;
		}
	}
}
void readin(){
	printf("Put In Your Name(English,between 3~15 letters):\n");
	scanf("%s",name);
	setpos(hHome);
	clearoutt();printf("\n");clearoutt();
	setpos(hHome);
}
int main(){
	srand(time(NULL));
	readin();
	HP=100;AMMO=400;kill=0;DA=6;
	for(int level=2;level<=2;level++){
		sprintf(map,"map%d.date",level);
		ifstream in(map);
		in>>n>>m>>ene>>bo>>kl;tttt=-1;
		for(int i=1;i<=n;i++){
			for(int j=1;j<=m;j++){
				in>>mp[i][j];
				if(mp[i][j]>10) bohp[i][j]=mp[i][j],ex[++tttt]=line(i,j);
				if(mp[i][j]==2) tx=i,ty=j;
			}
		}
		dr=1;
		ad=outbegin();
		kl+=ad*10;if(kl>100) kl=100;
		last=tlast=0;
		long long tim=0;
		bool ok=0,tok=0;int bos=0;
		clearout();
		while(ene){
			if(tim>=10000000007) tim=0;
			Sleep(50);
			setpos(hHome);
			if(kill==bos*bo){
				if(!tok&&bos){
					tok=1;
					randThing();
				}
				printf("Press X to continue!                                           \n");
				if(isPressKey('X')) ok=1;
				if(ok){
					clearout();
					tok=0;ok=0;
					bos++;
					for(int i=1,now=0;i<=bo;i+=now,ene-=now){
						now=randene();
					}
				}
			}
			out(tx-20,tx+20,ty-20,ty+20);
			check(++tim,ok);
			move(ok^1);
			if(HP<=0) break;
		}
		if(HP<=0) break;
		outpass(level,1);
		AMMO+=100;HP+=5;DA++;
		clearout();
	}
	outend();
	return 0;
}
