#include<scrlib.h>
#include<cstdio>
#include<cstdlib>
#include<ctime>
using namespace std;
using namespace ALL;
int map[111][111],bomtim[111][111];
const int inf=0x7fffffff;
struct ammo{
   int x,y;
   int ok;
   int hp;
   void clear(){x=y=ok=0;}
   void add(int a,int b){x=a,y=b;}
}shell[111][111],monster[111][111];
struct tower{
	int rds;int ok;
}tower[111][111];
const int P1=1,P2=22;
const int BOOM=100,AMMO=50;
int sc;
struct players{
	int x,y;
	int tx,ty;
	int life,money;
	int boom,ammo,da;
	char str[10];
}p1,p2;
void init(){
	map[1][1]=P1;
	p1.tx=p1.x=1;
	p1.ty=p1.y=1;
	p1.life=20;
	p1.da=7;
	p1.money=200;
	p1.boom=20;
	p1.ammo=500;
	p2=p1;
	for(int i=56;i<=57;i++){
		for(int j=1;j<=35;j++){
			map[j][i]=30;
		}
	}
}
void reborn(players &a,int id){
	map[a.x][a.y]=3;
	a.x=a.tx,a.y=a.ty;
	map[a.x][a.y]=id;
	a.boom+=20;a.ammo+=150;a.da=1;
	a.life--;
}
void rd(){
	int tx=0,ty=0;
	while(!tx||!ty||tx>35||ty>70||map[tx][ty]!=0){
		tx=rand()%35;ty=rand()%70;
	}
	map[tx][ty]=4;
}
void rd5(){
	int tx=0,ty=0;
	while(!tx||!ty||tx>35||ty>70||map[tx][ty]!=0){
		tx=rand()%35;ty=rand()%70;
	}
	map[tx][ty]=20;
}
void outplay(players a){
	printf("%s Life: %d Boom: %d Ammo: %d $:%d DA:%d SC: %d\n",a.str,a.life,a.boom,a.ammo,a.money,a.da,sc);
}
void checkammo(int now){
	for(int i=1;i<=35;i++){
		for(int j=1;j<=70;j++){
			if(shell[i][j].ok==now){
				int tx=shell[i][j].x,ty=shell[i][j].y;
				if(shell[i+tx][j+ty].ok!=0){
					tx+=tx;ty+=ty;
				}
				shell[i+tx][j+ty]=shell[i][j];
				shell[i+tx][j+ty].ok=now+1;
				shell[i][j].clear();
				if(monster[i][j].ok!=0){
					monster[i][j].hp-=p1.da;
					if(monster[i][j].hp<0) monster[i][j].clear(),sc++,p1.money+=p1.da*2,p2.money+=p2.da*2;
					shell[i+tx][j+ty].clear();
				}
				if(map[i][j]!=0&&map[i][j]!=P1&&map[i][j]!=30){	
					shell[i+tx][j+ty].clear();
				}
			}
		}
	}
}
void checkmon(){
	for(int i=1;i<=35;i++){
		for(int j=1;j<=70;j++){
			if(monster[i][j].ok==1){
				if(map[i][j]==30){
					map[i][j]=0;
					monster[i][j].clear();
					p1.money+=5;sc++;
				}
				if(bomtim[i][j]!=0){
					bomtim[i][j]=0;
					monster[i][j].clear();
				}
				if(tower[i][j].ok==1){
					monster[i][j].clear();
					tower[i][j].rds=0;
					tower[i][j].ok=0;
					continue;					
				}
				if(shell[i][j].ok!=0){
					shell[i][j].clear();sc++,p1.money+=p1.da*2,p2.money+=p2.da*2;
					monster[i][i].hp-=p1.da;
					if(monster[i][j].hp<0){
						monster[i][j].clear();
					}
					continue;
				}				
				if(map[i][j]==P1){
					reborn(p1,P1);
				}
				if(map[i][j]==P2){
					reborn(p2,P2);
				}					
				if(map[i][j]==3){
					monster[i][j].clear();continue;
				}
				int tx=monster[i][j].x,ty=monster[i][j].y;
				if(monster[i+tx][j+ty].ok!=0){
					tx+=tx;ty+=ty;
				}
				monster[i+tx][j+ty]=monster[i][j];
				monster[i][j].clear();
			}
		}
	}
}
void rd2(int hp){
	int now=0;
	while(!now||now>35||monster[now][70].ok!=0||map[now][70]!=0){now=rand()%35;}
	monster[now][70].ok=1;monster[now][70].add(0,-1);monster[now][70].hp=hp;
}
int play;
void out(){
	setpos(hHome);
	for(int i=1;i<=35;i++){
		for(int j=1;j<=70;j++){
			if(shell[i][j].ok){
				setyellow();
				printf("*");
				setblank();
				continue;
			}
			if(tower[i][j].ok){
				setgreen();
				printf(">");
				setblank();
				continue;
			}
			if(monster[i][j].ok){
				setred();
				printf("+");
				setblank();
				continue;
			}
			if(map[i][j]==10){
				setyellow();
				printf("&");
				setblank();
			}
			else if(map[i][j]==30){
				printf("E");
			}
			else if(map[i][j]==20){
				setyellow();
				printf("A");
				setblank();
			}
			else if(map[i][j]==3){
				setwhite();
				printf("@");
				setblank();
			}
			else if(map[i][j]==5){
				setred();
				printf("*");
				setblank();
			}
			else if(map[i][j]==P1){
				setwater();
				printf("O");
				setblank();
			}
			else if(map[i][j]==P2){
				setwater();
				printf("P");
				setblank();
			}			
			else if(map[i][j]==4){
				setgreen();
				printf("#");
				setblank();
			}else if(bomtim[i][j]!=0){
				setpink();
				printf("#");
				setblank();
			}else printf(" ");
		}
		printf("\n");
	}
	setyellow();
	outplay(p1);
	if(play==2)
	outplay(p2);
	setblank();
}
int dx[]={-1,0,0,1,-1,1,1,-1},dy[]={0,-1,1,0,-1,1,-1,1};
void check(){
	for(int i=1;i<=35;i++){
		for(int j=1;j<=70;j++){
			if(bomtim[i][j]!=0&&map[i][j]!=3){
				bomtim[i][j]--;
				if(bomtim[i][j]==0){
					if(map[i][j]==P1){
						reborn(p1,P1);
					}
					if(map[i][j]==P2){
						reborn(p2,P2);
					}					
					if(monster[i][j].ok!=0){
					monster[i][j].clear();
					}					
					if(map[i][j]!=3)					
					map[i][j]=5;
					for(int k=0;k<8;k++){
						int tx=i+dx[k],ty=j+dy[k];
						if(map[tx][ty]==P1){
							reborn(p1,P1);
						}
						if(monster[tx][ty].ok!=0){
							monster[tx][ty].clear();
						}
						if(map[tx][ty]!=3)
						map[tx][ty]=5;
					}
					out();
					for(int k=0;k<8;k++){
						int tx=i+dx[k],ty=j+dy[k];
						if(map[tx][ty]!=3)
						map[tx][ty]=0;
					}
					if(map[i][j]!=3)
					map[i][j]=0;					
				}
			}
		}
	}
}
int ww;
void checktower(){
	for(int i=1;i<=35;i++){
		for(int j=1;j<=70;j++){
			if(tower[i][j].ok){
				if(!tower[i][j].rds){
					shell[i][j+1].ok=ww+1;
					shell[i][j+1].add(0,1);
					tower[i][j].rds=10;
				}else{
					tower[i][j].rds--;
				}
			}
		}
	}
}
int main()
{

	srand(time(0));
	printf("Input Your names:1 single 2 double\n");
	scanf("%d",&play);
	scanf("%s",p1.str);
	if(play==2){
		scanf("%s",p2.str);
	}
	int leve=5;
	clearout();
	init();
	ww=1;
	int N=10000007,cnt=0,w=0,timr2=0,kw=5;
	while(1){
		
		cnt++;w++;timr2++;
		if(w%(N-5000)==0){
		w=0;checkammo(++ww);checkmon();
		}
		if(timr2%(N*13+p1.da*1000)==0){
			rd2(kw),timr2=0;kw++;
		}
		if(cnt%N!=0) continue;
		map[p1.tx][p1.ty]=10;
		cnt=0;kw--;
		check();checktower();
		if(p1.ammo>0){
		if(isPressKey('U')){
			shell[p1.x-1][p1.y].ok=ww+1;
			shell[p1.x-1][p1.y].add(-1,0);
			p1.ammo--;
		}
		else if(isPressKey('J')){
			shell[p1.x+1][p1.y].ok=ww+1;
			shell[p1.x+1][p1.y].add(1,0);			
		p1.ammo--;
		}
		else if(isPressKey('H')){
			shell[p1.x][p1.y-1].ok=ww+1;
			shell[p1.x][p1.y-1].add(0,-1);			
		p1.ammo--;
		}
		else if(isPressKey('K')){
			shell[p1.x][p1.y+1].ok=ww+1;
			shell[p1.x][p1.y+1].add(0,1);			
		p1.ammo--;
		}			
		}					
		if(isPressKey('W')){
			int tx=p1.x,ty=p1.y;
			map[p1.x][p1.y]=0;
			p1.x=max(1,p1.x-1);
			if(map[p1.x][p1.y]!=0&&map[p1.x][p1.y]!=4&&map[p1.x][p1.y]!=20){
                map[tx][ty]=P1;
                p1.x=tx,p1.y=ty;
                continue;
			}
            if(map[p1.x][p1.y]==4){
				p1.boom++;
			}
			if(map[p1.x][p1.y]==20){
				p1.ammo+=50;
			}			
			map[p1.x][p1.y]=P1;
		} 
		else if(isPressKey('S')){
			int tx=p1.x,ty=p1.y;
			map[p1.x][p1.y]=0;
			p1.x=min(35,p1.x+1);
			if(map[p1.x][p1.y]!=0&&map[p1.x][p1.y]!=4&&map[p1.x][p1.y]!=20){
                map[tx][ty]=P1;
                p1.x=tx,p1.y=ty;
                continue;
			}
            if(map[p1.x][p1.y]==4){
				p1.boom++;
			}
			if(map[p1.x][p1.y]==20){
				p1.ammo+=50;
			}			
			map[p1.x][p1.y]=P1;		
		}
		else if(isPressKey('A')){
		int tx=p1.x,ty=p1.y;
			map[p1.x][p1.y]=0;
			p1.y=max(1,p1.y-1);
			if(map[p1.x][p1.y]!=0&&map[p1.x][p1.y]!=4&&map[p1.x][p1.y]!=20){
                map[tx][ty]=P1;
                p1.x=tx,p1.y=ty;
                continue;
			}
            if(map[p1.x][p1.y]==4){
				p1.boom++;
			}
			if(map[p1.x][p1.y]==20){
				p1.ammo+=50;
			}			
			map[p1.x][p1.y]=P1;				
		}
		else if(isPressKey('D')){
			int tx=p1.x,ty=p1.y;
			map[p1.x][p1.y]=0;
			p1.y=min(70,p1.y+1);
			if(map[p1.x][p1.y]!=0&&map[p1.x][p1.y]!=4&&map[p1.x][p1.y]!=20){
                map[tx][ty]=P1;
                p1.x=tx,p1.y=ty;
                continue;
			}
            if(map[p1.x][p1.y]==4){
				p1.boom++;
			}
			if(map[p1.x][p1.y]==20){
				p1.ammo+=50;
			}
			map[p1.x][p1.y]=P1;					
		}
		if(isPressKey('B')){
			if(p1.boom>0){	
		    bomtim[p1.x][p1.y]=10;
			p1.boom--;
			}
		}
		if(isPressKey('F')){
			if(p1.money>=100){
			rd();p1.money-=100;				
			}
		}
		if(isPressKey('G')){
			if(p1.money>=50){
			rd5();p1.money-=50;				
			}
		}
		if(isPressKey('T')){
			if(p1.money>=200){
				tower[p1.x][p1.y].ok=1;
				tower[p1.x][p1.y].rds=7;
				p1.money-=200;
			}
		}
	if(play==2){
        if(isPressKey(38)){
	        int tx=p2.x,ty=p2.y;
			map[p2.x][p2.y]=0;
			p2.x=max(1,p2.x-1);
			if(map[p2.x][p2.y]!=0&&map[p2.x][p2.y]!=4){
                map[tx][ty]=P2;
                p2.x=tx,p2.y=ty;
                continue;
			}
            if(map[p2.x][p2.y]==4){
				p2.boom++;
			}
			map[p2.x][p2.y]=P2;				
		}
		else if(isPressKey(40)){
	        int tx=p2.x,ty=p2.y;
			map[p2.x][p2.y]=0;
			p2.x=min(35,p2.x+1);
			if(map[p2.x][p2.y]!=0&&map[p2.x][p2.y]!=4){
                map[tx][ty]=P2;
                p2.x=tx,p2.y=ty;
                continue;
			}
            if(map[p2.x][p2.y]==4){
				p2.boom++;
			}
			map[p2.x][p2.y]=P2;			
		}
		else if(isPressKey(37)){
	        int tx=p2.x,ty=p2.y;
			map[p2.x][p2.y]=0;
			p2.y=max(1,p2.y-1);
			if(map[p2.x][p2.y]!=0&&map[p2.x][p2.y]!=4){
                map[tx][ty]=P2;
                p2.x=tx,p2.y=ty;
                continue;
			}
            if(map[p2.x][p2.y]==4){
				p2.boom++;
			}
			map[p2.x][p2.y]=P2;				
		}
		else if(isPressKey(39)){
	        int tx=p2.x,ty=p2.y;
			map[p2.x][p2.y]=0;
			p2.y=min(70,p2.y+1);
			if(map[p2.x][p2.y]!=0&&map[p2.x][p2.y]!=4){
                map[tx][ty]=P2;
                p2.x=tx,p2.y=ty;
                continue;
			}
            if(map[p2.x][p2.y]==4){
				p2.boom++;
			}
			map[p2.x][p2.y]=P2;				
		}
		if(isPressKey(101)){
			shell[p2.x-1][p2.y].ok=ww+1;			
			shell[p2.x-1][p2.y].add(-1,0);
		}
		else if(isPressKey(98)){
			shell[p2.x+1][p2.y].ok=ww+1;			
			shell[p2.x+1][p2.y].add(1,0);			
		}
		else if(isPressKey(97)){
			shell[p2.x][p2.y-1].ok=ww+1;		
			shell[p2.x][p2.y-1].add(0,-1);			
		}
		else if(isPressKey(99)){
			shell[p2.x][p2.y+1].ok=ww+1;		
			shell[p2.x][p2.y+1].add(0,1);			
		}
		if(isPressKey(96)){
			if(p2.boom>0){rd();
			bomtim[p2.x][p2.y]=10;
			p2.boom--;				
			}
		}
		if(isPressKey(100)){
			if(p2.money>=200){
				tower[p1.x][p1.y].ok=1;
				tower[p1.x][p1.y].rds=7;
				p2.money-=200;
			}
		}
		if(isPressKey(103)){
			if(p2.money>=100){
			rd();p2.money-=100;				
			}
		}
		if(isPressKey(104)){
			if(p2.money>=50){
			rd5();p2.money-=50;				
			}
		}
     }
		if(sc>=leve){
			leve+=10;p1.da+=5;p1.ammo+=10;
			p2.da+=5,p2.ammo+=10;
		}
		out();
		if(p1.life==0) break;								    
	}
	clearout();
	if(p1.life>0) printf("%s win! Score :\n",p1.str,sc);
	printf("Press E to continue......\n");
	while(1){
		if(isPressKey('E')) return 0;
	}
	return 0;
}

