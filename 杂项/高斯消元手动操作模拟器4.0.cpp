#include <cstdio>
#include <cstring>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include "windows.h"
using namespace std;
int mat[50][50],a[50][50],x[50];
double data[50];
bool free_x[50]; 
int n,m;
char cao[10],cao2[10];
void changeline(){
SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_INTENSITY|FOREGROUND_RED|FOREGROUND_GREEN);	
	for(int i=1;i<=m+1;i++) printf("-------");
	printf("\n");
SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_INTENSITY);	
}
void changenext(){
SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_INTENSITY|FOREGROUND_RED);	
	for(int i=1;i<=m+1;i++) printf("-=-=-");
	printf("\n");
SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_INTENSITY);		
}
void out(){
changenext();	
    changeline();
	for(int i=1;i<=n;i++){
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_INTENSITY|FOREGROUND_RED);
		printf("|");
		for(int j=1;j<=m;j++){
			printf(" %3d |",mat[i][j]);
		}
		printf("= %3d\n",mat[i][m+1]);
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_INTENSITY);	
		changeline();
	}
	changeline();
changenext();		
}
int ans[40];
int rd1(){
	int k=0;
	while(k<=2||k>5){
		k=rand()%7;
	}
	return k;
}
int rd2(){
	int w=0;
	while(w>=n+2||!w){
		w=rand()%n+2;
    }
	return n-1;
}
int rd3(){
	int k=1000;
	while(k>=10){
		k=rand()%11+rand()%5;
	}
	return k;
}
int rd4(){
	int w=99999;
	while(w>100){
		w=rand()%200+rand()%100-rand()%100;
	}
	return w;
}
int www,kkk;
void init(){
	n=rd1();m=rd2();
	for(int i=1;i<=n;i++){
		ans[i]=rd4();
	}
	if(cao2[0]=='C')
	www=rd4();
	if(cao2[0]!='B'){
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			mat[i][j]=rd3();
			mat[i][m+1]+=ans[j]*mat[i][j];
		}
	}	
	if(cao2[0]=='C'){
		int w=rand()%n;
		if(!w) w++;
		mat[w][m+1]+=www;
	}	
	}else{
	   for(int i=1;i<=max(n-3,2);i++){
	   	for(int j=1;j<=m;j++)
	   	  mat[i][j]=rd3();
		mat[i][m+1]=rd4();	 	   	  
	   }
	   int cnt=0;
	   for(int i=max(n-3,2)+1;i<=n;i++){
	   	int www=rand()%(max(n-3,2));
	   	if(!www) www=1;cnt++;
	   	for(int j=1;j<=m;j++){
	   		mat[i][j]=mat[www][j]*cnt;
	   	}
	   	mat[i][m+1]=mat[www][m+1]*cnt;
	   }	
	}
	memcpy(a,mat,sizeof(mat));
	printf("%d %d\n",n,m);
}
int rd6(){
	int kl=rand()%100;
	if(kl>=50) return 0;
	else return 1;
}
#include<cmath>
void init222(){
	n=rd1();m=rd2();
	for(int i=1;i<=n;i++) ans[i]=abs(rd4());
	for(int i=1;i<=n;i++){
		int now=0;
		for(int j=1;j<=m;j++){
			mat[i][j]=rd6();
			now+=mat[i][j]*ans[j];
		}
		mat[i][m+1]=now%2;
	}
	printf("%d %d\n",n,m);
}

int gcd(int a,int b){
    if(!b) return a;
    return gcd(b,a%b);
}
int lcm(int a,int b){
	return a/gcd(a,b)*b;
}

// 高斯消元法解方程组(Gauss-Jordan elimination).(-2表示有浮点数解，但无整数解，
//-1表示无解，0表示唯一解，大于0表示无穷解，并返回自由变元的个数)
//有 equ个方程，var个变元。增广矩阵行数为equ,分别为0到equ-1,列数为var+1,分别为0到var.
int Gauss()
{
    int i,j,k;
    int max_r;// 当前这列绝对值最大的行.
    int col;//当前处理的列
    int ta,tb;
    int LCM;
    int temp;
    int free_x_num;
    int free_index;

    for(int i=1;i<=m;i++)
    {
        x[i]=0;
        free_x[i]=true;
    }

    //转换为阶梯阵.
    col=0; // 当前处理的列
    for(k = 1;k <= n && col <= m;k++,col++)
    {// 枚举当前处理的行.
// 找到 该col列元素绝对值最大的那行与第k行交换.(为了在除法时减小误差)
        max_r=k;
        for(i=k+1;i<=n;i++)
        {
            if(abs(a[i][col])>abs(a[max_r][col])) max_r=i;
        }
        if(max_r!=k)
        {// 与第k行交换.
            for(j=k;j<=m+1;j++) swap(a[k][j],a[max_r][j]);
        }
        if(a[k][col]==0)
        {// 说明该col列第k行以下全是0了，则处理当前行的下一列.
            k--;
            continue;
        }
        for(i=k+1;i<=n;i++)
        {// 枚举要删去的行.
            if(a[i][col]!=0)
            {
                LCM = lcm(abs(a[i][col]),abs(a[k][col]));
                ta = LCM/abs(a[i][col]);
                tb = LCM/abs(a[k][col]);
                if(a[i][col]*a[k][col]<0)tb=-tb;//异号的情况是相加
                for(j=col;j<=m+1;j++)
                {
                    a[i][j] = a[i][j]*ta-a[k][j]*tb;
                }
            }
        }
    }

  //  Debug();

    // 1. 无解的情况: 化简的增广阵中存在(0, 0, ..., a)这样的行(a != 0).
    for (i = k; i <=n; i++)
    { // 对于无穷解来说，如果要判断哪些是自由变元，那么初等行变换中的交换就会影响，则要记录交换.
        if (a[i][col] != 0) return -1;
    }
    // 2. 无穷解的情况: 在var * (var + 1)的增广阵中出现(0, 0, ..., 0)这样的行，即说明没有形成严格的上三角阵.
    // 且出现的行数即为自由变元的个数.
    if (k < m-1)
    {
        // 首先，自由变元有var - k个，即不确定的变元至少有var - k个.
        for (i = k; i >= 1; i--)
        {
            // 第i行一定不会是(0, 0, ..., 0)的情况，因为这样的行是在第k行到第equ行.
            // 同样，第i行一定不会是(0, 0, ..., a), a != 0的情况，这样的无解的.
            free_x_num = 0; // 用于判断该行中的不确定的变元的个数，如果超过1个，则无法求解，它们仍然为不确定的变元.
            for (j = 1; j <=m ; j++)
            {
                if (a[i][j] != 0 && free_x[j]) free_x_num++, free_index = j;
            }
            if (free_x_num > 1) continue; // 无法求解出确定的变元.
            // 说明就只有一个不确定的变元free_index，那么可以求解出该变元，且该变元是确定的.
            temp = a[i][m+1];
            for (j = 0; j <= m; j++)
            {
                if (a[i][j] != 0 && j != free_index) temp -= a[i][j] * x[j];
            }
            x[free_index] = temp / a[i][free_index]; // 求出该变元.
            free_x[free_index] = 0; // 该变元是确定的.
        }
        return m - k; // 自由变元有var - k个.
    }
    // 3. 唯一解的情况: 在var * (var + 1)的增广阵中形成严格的上三角阵.
    // 计算出Xn-1, Xn-2 ... X0.
    for (i = m; i >= 1; i--)
    {
        temp = a[i][m+1];
        for (j = i + 1; j <= m; j++)
        {
            if (a[i][j] != 0) temp -= a[i][j] * x[j];
        }
        if (temp % a[i][i] != 0) return -2; // 说明有浮点数解，但无整数解.
        x[i] = temp / a[i][i];
    }
    return 0;
}
int check1(int a){
	if(a>1) return 0;
	if(!a){
		for(int i=1;i<=n;i++){
			int g=0;
		for(int j=1;j<=m;j++){
			if(mat[i][j]) g=1;
			if(mat[i][j]!=0&&j<i) return 3;
		}	if(!g&&mat[i][m+1]!=0) return 2;
		    if(!g) return 1;		
		}
		int cnt=1;
		data[n]=mat[n][m+1]/mat[n][n];
//		printf("K %d %d %d\n",n,mat[n][n],mat[n][m+1]);
		for(int i=n-1;i>=1;i--){
			double temp=mat[i][m+1];
			for(int j=1;j<=m;j++){
				if(mat[i][j]!=0&&j!=i) temp-=mat[i][j]*data[j];
			}
//			printf("K %d %d %d\n",i,mat[i][i],temp);
			data[i]=temp/mat[i][i];cnt++;
			if(cnt==n) return 4;
		}
	}		
	else{
		for(int i=1;i<=n;i++){
			int g=0;
			if(mat[i][i]==0) return 3;
		for(int j=1;j<=m;j++){
			if(mat[i][j]) g=1;			
			if(i!=j&&mat[i][j]!=0) return 3;
		}	if(!g&&mat[i][m+1]!=0) return 2;
		    if(!g) return 1;		
		}
		for(int i=1;i<=n;i++){
			data[i]=mat[i][m+1]/mat[i][i];
		}
		return 4;
	}
}
int calc(int a){
	int now=check1(a);
	if(now==0) return 0;
	if(now==2) {
		printf("无解\n");
		return 0;
	}
	if(now==1){
		printf("无穷解\n");
		return 0;
	}
	if(now==3) {
		printf("不符合形式\n"); 
	    return 0;
	}
	if(now==4){
		return 1;
	}
}
void method(){
SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_INTENSITY|FOREGROUND_GREEN|FOREGROUND_BLUE);	
    printf("M i w j k:将第i行乘以w减去第j行乘以k\n");
    printf("W i w j k:将第i行乘以w加上第j行乘以k\n");
	printf("K :输入自己计算的答案（就不用F了）\n"); 
	printf("F i:结束并输出你所求得的答案（如果无法求出则无输出）i=0 就是高斯消元最后形式（倒三角） i=1 就是若尔当消元最后形式（对角线）\n");
	printf("使用以下操作先输入Z和(空格)\n"); 
	printf("i * j :将第i行乘以j\n");
	printf("i / j :将第i行除以j(仅支持整除)\n");
	printf("i + j :将第i行加上第j行\n");
	printf("i - j :将第i行减去第j行\n");
	printf("i C j :交换第i行和第j行元素\n");
	printf("i G j :求i和j的最大公因数\n");
	printf("i L j :求i和j的最小公倍数\n");
    if(cao2[0]=='D'){
SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_INTENSITY|FOREGROUND_GREEN|FOREGROUND_GREEN);
	printf("以下是解异或方程用的：请先输入R和(空格)然后使用下列操作\n");
	printf("i ^ j :第i行异或第j行\n");
	printf("i & j :第i行与第j行\n");
	printf("i | j :第i行或第j行\n");
	printf("不支持位移操作\n");       	
	}
SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_INTENSITY);	
}
bool flag;
char cao3[10];
int t1,t2;
int w1,k1;
void check44(){
	scanf("%s",cao3);
	scanf("%d%d%d%d",&t1,&w1,&t2,&k1);
	char a=cao3[0];
	if(a=='M'){
		for(int i=1;i<=m+1;i++){
			mat[t1][i]*=w1;
			mat[t1][i]-=mat[t2][i]*k1;
		}
	}
	if(a=='W'){
		for(int i=1;i<=m+1;i++){
			mat[t1][i]*=w1;
			mat[t1][i]+=mat[t2][i]*k1;
		}	
	}		
}

int Gauss_Xor(){
	int i,j,k;
	for(int now=1;now<=m;now++){
		for(i=now;i<=n&&!a[i][now];i++);
		if(i==n+1){return 0;}
		k=max(k,i);
		swap(a[now],a[i]);
		for(j=1;j<=n;j++){
			if(j!=now&&a[j][now]==1){
				for(int w=1;w<=m+1;w++)
			    a[j][w]^=a[now][w];
			}
		}
	}
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m+1;j++){
			printf("%d ",a[i][j]);
		}   printf("\n");
	}
	for(i=1;i<=m;i++) ans[i]=a[i][m+1]%2;
	return 1;
}
bool okk;
void check(){
	scanf("%d%s%d",&t1,cao3,&t2);
	char a=cao3[0];
	okk=1;
	if(a=='*'){
		for(int i=1;i<=m+1;i++){
			mat[t1][i]*=t2;
		}
		return;
	}
	if(a=='/'){
		for(int i=1;i<=m+1;i++){
			mat[t1][i]/=t2; 
		}
		return;
	} 
	if(a=='+'){
		for(int i=1;i<=m+1;i++){
			mat[t1][i]+=mat[t2][i];
		}
		return;
	}
	if(a=='-'){
		for(int i=1;i<=m+1;i++){
			mat[t1][i]-=mat[t2][i];
		}
	}
	if(a=='C'){
		for(int i=1;i<=m+1;i++){
			swap(mat[t1][i],mat[t2][i]);
		}
	}
	if(a=='G'){
		okk=0;
		printf("GCD is %d\n",gcd(t1,t2));
	}
	if(a=='L'){
		okk=0;
		printf("LCM is %d\n",lcm(t1,t2));
	}	
}
int t3,t4;
char cao4[10];
void check22(){
	if(cao4[0]=='|'){
		for(int i=1;i<=m+1;i++)
		mat[t3][i]|=mat[t4][i];
	}
	if(cao4[0]=='&'){
		for(int i=1;i<=m+1;i++)
		mat[t3][i]&=mat[t4][i];
	}
	if(cao4[0]=='^'){
		for(int i=1;i<=m+1;i++)
		mat[t3][i]^=mat[t4][i];
	}
}
int main()
{
SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_INTENSITY|FOREGROUND_GREEN|FOREGROUND_RED);		
	printf("Copyright Oc Czt 2018 TM\n All rights reserved \n");
	printf("Made by CrazyTeaMajor Flamingo 4.0\n");

SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_INTENSITY|FOREGROUND_RED);	
	printf("欢迎使用高斯消元计算辅助器！请不要输入无效指令或数据以防卡崩！\n");	
begin:
SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_INTENSITY|FOREGROUND_GREEN);	
	printf("请选择模式：A随机,B输入\n");
	scanf("%s",cao);
	if(cao[0]=='A'){
		srand(time(0));
		printf("请选择随机模式：A有唯一解,B无限解,C无解,D异或有解方程\n");
		scanf("%s",cao2);
		if(cao2[0]=='D'){
			init222();
		} else init();
		memcpy(a,mat,sizeof(mat));
		out();
	}else{
	printf("请输入矩阵大小：(方程个数和未知数个数)\n");
	scanf("%d%d",&n,&m);
	m++;
	printf("请输入矩阵：\n");
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			scanf("%d",&mat[i][j]);
		}
	} 
	memcpy(a,mat,sizeof(mat));
	m--;		
	}
	int kk=0;
	while(1)
	{
SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_INTENSITY|FOREGROUND_RED|FOREGROUND_GREEN|FOREGROUND_BLUE);		
		printf("请输入操作：\n");
		printf("S 终止\n");
		method();
		scanf(" %s",cao3);
		if(cao3[0]=='S') break;
		if(cao3[0]=='R'){
			scanf("%d%s%d",&t3,cao4,&t4);
			check22();
			out();
			continue;
		}
		if(cao3[0]=='Z'){
			check();
			if(okk)out(); 
			continue;
		} 
		if(cao3[0]=='F'){
			int w;
			scanf("%d",&w);
			flag=calc(w); 
			if(flag)
			for(int i=1;i<=n;i++) printf("%.1lf ",data[i]);printf("\n");
			break;
		}
		if(cao3[0]=='K'){
			flag=1;kk=1;
			for(int i=1;i<=n;i++){
				scanf("%lf",&data[i]);
			}
			break;
		}
		check44();
//		check(cao3[0]);
		
	}
SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_INTENSITY|FOREGROUND_RED|FOREGROUND_BLUE);	
	printf("\n<><><><><><><><><>参考(可能不对)<><><><><><><><><><>\n");
SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_INTENSITY|FOREGROUND_RED);
for(int i=1;i<=m;i++) printf("%d ",ans[i]);printf("\n");
	int ww=0;
	if(flag&&cao2[0]!='D'){
	int free_num=Gauss();
	if (free_num == -1) printf("无解!\n");
    else if (free_num == -2) printf("有浮点数解，无整数解!\n");
        else if (free_num > 0)
        {
            printf("无穷多解! 自由变元个数为%d\n", free_num);
            for (int i = 1; i <= m; i++)
            {
                if (free_x[i]) printf("x%d 是不确定的\n", i);
                else printf("x%d: %d\n", i , x[i]);
            }
        }
        else
        {
            for (int i = 1; i <= m; i++)
            {
                printf("x%d: %d\n", i, x[i]);
                if(x[i]!=(int)data[i]) ww=1;
            }
        }
	if(free_num>0){
	if(ww&&kk) printf("你的答案错了！\n");
	else if(kk) printf("你算对了！\n");		
	}		
	}
	if(cao2[0]=='D'){
		printf("0为偶数，1位奇数\n"); 
		int ttt=Gauss_Xor();
		if(ttt==1)
		for(int i=1;i<=m;i++) printf("x%d=%d\n",i,ans[i]);
		else printf("无数多组解");
		printf("\n");
	}
	char str[10];
	printf("是否重来？Y：Yes，N：No？\n");
	scanf("%s",str);
	if(str[0]=='Y') goto begin;
	printf("Press Any Key To Shutdown......\n");
        getchar();getchar();getchar();
SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_INTENSITY);        
   return 0; 
}
