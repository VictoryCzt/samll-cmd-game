#include<cstring>
#include<cstdlib>
#include<iostream>
#include<cstdio>
#include<ctime>

using namespace std;
int map[6][6],n;
long long score;
void change(int &a,int &b){int t=a;a=b;b=t;}
void out(){
	for(int i=1;i<=35;i++) printf("=");printf("\n");
	for(int i=1;i<=n;i++)
	{
		printf("|");
		for(int j=1;j<=n;j++)
		{
			if(map[i][j]!=0) printf(" %4d |",map[i][j]);
			else printf("      |");
		}
		printf("\n");
		for(int j=1;j<=n;j++)
		printf("-------");
		printf("\n");
	}
	for(int i=1;i<=35;i++) printf("=");printf("\n");
	printf("Your score : %lld\n",score);
}
void rd(){
	srand(time(0));
	int x=0,y=0;
	while(!x||x>n||map[x][y]||y>n||!y){
		x=rand()%n+1;y=rand()%n+1;
	}
	int kl=rand()%100;
	if(kl>=64) map[x][y]=4;
	else map[x][y]=2;
}
int dx[]={1,0,0,-1},dy[]={0,1,-1,0};
bool ok;
bool check(){
	bool ok=0;
	for(int i=1;i<=n;i++)
	for(int j=1;j<=n;j++){
		if(!map[i][j]) ok=1;
		else
		for(int k=0;k<=3;k++){
			int x=i+dx[k],y=j+dy[k];
			if(x>=1&&x<=n&&y>=1&&y<=n){
				if(map[x][y]==map[i][j]) ok=1;
			}
		}
	}
	return ok;
}
void up(){
	for(int j=1;j<=n;j++)
	for(int i=1;i<=n;i++){
		if(map[i][j]){
			int pos=i;
			for(int k=i-1;k>=1;k--){
				if(!map[k][j]){
					change(map[k][j],map[pos][j]);
					pos=k;ok=1;
				}else break;
			}
		}
	}
	for(int j=1;j<=n;j++)
	for(int i=2;i<=n;i++){
		if(map[i][j]&&map[i-1][j])
		if(map[i][j]==map[i-1][j]){
			map[i-1][j]+=map[i][j];
			map[i][j]=0;
			score+=map[i-1][j];ok=1;
		}
	}
	for(int j=1;j<=n;j++)
	for(int i=1;i<=n;i++){
		if(map[i][j]){
			int pos=i;
			for(int k=i-1;k>=1;k--){
				if(!map[k][j]){
					change(map[k][j],map[pos][j]);
					pos=k;ok=1;
				}else break;
			}
		}
	}	
}
void down(){
	for(int j=1;j<=n;j++)
	for(int i=n;i>=1;i--){
		if(map[i][j]){
			int pos=i;
			for(int k=i+1;k<=n;k++){
				if(!map[k][j]){
					change(map[k][j],map[pos][j]);
					pos=k;ok=1;
				}else break;
			}
		}
	}
	for(int j=1;j<=n;j++)
	for(int i=n;i>=2;i--){
		if(map[i][j]&&map[i-1][j])
		if(map[i][j]==map[i-1][j]){
			map[i][j]+=map[i-1][j];
			map[i-1][j]=0;
			score+=map[i][j];ok=1;
		}
	}
	for(int j=1;j<=n;j++)
	for(int i=n;i>=1;i--){
		if(map[i][j]){
			int pos=i;
			for(int k=i+1;k<=n;k++){
				if(!map[k][j]){
					change(map[k][j],map[pos][j]);
					pos=k;ok=1;
				}else break;
			}
		}
	}
}
void left(){
	for(int i=1;i<=n;i++)
	for(int j=n;j>=1;j--){
		if(map[i][j]){
			int pos=j;
			for(int k=j-1;k>=1;k--){
				if(!map[i][k]){
					change(map[i][k],map[i][pos]);
					pos=k;ok=1;
				}else break;
			}
		}
	}
	for(int i=1;i<=n;i++)
	for(int j=2;j<=n;j++){
		if(map[i][j]&&map[i][j-1])
		if(map[i][j]==map[i][j-1]){
			map[i][j-1]+=map[i][j];
			map[i][j]=0;
			score+=map[i][j-1];ok=1;
		}
	}
	for(int i=1;i<=n;i++)
	for(int j=n;j>=1;j--){
		if(map[i][j]){
			int pos=j;
			for(int k=j-1;k>=1;k--){
				if(!map[i][k]){
					change(map[i][k],map[i][pos]);
					pos=k;ok=1;
				}else break;
			}
		}
	}	
}
void right(){
	for(int i=1;i<=n;i++)
	for(int j=1;j<=n;j++){
		if(map[i][j]){
			int pos=j;
			for(int k=j+1;k<=n;k++){
				if(!map[i][k]){
					change(map[i][k],map[i][pos]);
					pos=k;ok=1;
				}else break;
			}
		}
	}
	for(int i=1;i<=n;i++)
	for(int j=n;j>=2;j--){
		if(map[i][j]&&map[i][j-1])
		if(map[i][j]==map[i][j-1])
		{
			map[i][j]+=map[i][j-1];
			map[i][j-1]=0;
			score+=map[i][j];
			ok=1;
		}
	}
	for(int i=1;i<=n;i++)
	for(int j=1;j<=n;j++){
		if(map[i][j]){
			int pos=j;
			for(int k=j+1;k<=n;k++){
				if(!map[i][k]){
					change(map[i][k],map[i][pos]);
					pos=k;ok=1;
				}else break;
			}
		}
	}
	for(int i=1;i<=n;i++)
	for(int j=1;j<=n;j++){
		if(map[i][j]){
			int pos=j;
			for(int k=j+1;k<=n;k++){
				if(!map[i][k]){
					change(map[i][k],map[i][pos]);
					pos=k;ok=1;
				}else break;
			}
		}
	}			
}


char cao[10];
int main()
{
	ok=1;
	printf("=============2048==============\n");
	printf("                                 Made by CZT\n");
	printf("输入难度：A简单，B普通，C困难\n");
	scanf("%s",cao);
	if(cao[0]=='A') n=5;
	if(cao[0]=='B') n=4;
	if(cao[0]=='C') n=3; 
	while(check()){
		if(ok)rd();
		ok=0;
		out();
		printf("\n输入操作：W上S下A左D右\n");
		scanf("%s",cao);
		if(cao[0]=='w'||cao[0]=='W'){
			up();
		}else if(cao[0]=='S'||cao[0]=='s'){
			down();
		}else if(cao[0]=='A'||cao[0]=='a'){
			left();
		}else if(cao[0]=='D'||cao[0]=='d'){
			right();
		}
	}
	printf("\nYour Final Score :%lld\n",score);
	freopen("YourScore.txt","w",stdout);
	printf("Score: %d\n",score);
	fclose(stdout);
	return 0;
}
