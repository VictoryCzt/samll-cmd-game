#include <windows.h>
#include <ctime>
#include <tchar.h>
#include <cstdlib>
#include <stdlib.h>
#include <wingdi.h>

using namespace std;
enum{
	ID_stop,ID_con,ID_reset,ID_sta,ID_TIM,ID_close,ID_TIMER
};


HWND thour,mint,sced,usce,info;
HWND stop,cont,reset,sta,close;
HINSTANCE hin;
int sit;
int h,m,s,u,cnt;
void clear(){
	h=m=s=u=0;
	SetWindowText(thour,"00");
	SetWindowText(mint,"00");
	SetWindowText(sced,"00");
	SetWindowText(usce,"000");
}
char ho[20],mi[20],sc[10],uc[10];
bool ok;

VOID CALLBACK Timers(HWND hwnd,UINT Mes,UINT wp,DWORD lp){
	if(sit==1){//计时
		Sleep(20);
		u+=2;
		s+=u/100;
		m+=s/60;
		h+=m/60;
		wsprintf(ho,"%02d",h%60);
		wsprintf(mi,"%02d",m%60);
		wsprintf(sc,"%02d",s%60);
		wsprintf(uc,"%03d",u%100);
		if(u>=100){
		u%=100;
		if(m>=60)SetWindowText(thour,ho),m%=60;
		if(s>=60)SetWindowText(mint,mi),s%=60;
		SetWindowText(sced,sc);
		}
		SetWindowText(usce,uc);
	}
}
VOID CALLBACK TimerProc(HWND hwnd, UINT message, UINT iTimerID, DWORD dwTime)
{
    HBRUSH          hBrush;
    HDC             hdc;
    RECT            rc;

    MessageBeep(-1);

    GetClientRect(hwnd, &rc);

    hdc     = GetDC(hwnd);
    hBrush = CreateSolidBrush(RGB(rand()%255, rand()%255, rand()%255));
    FillRect(hdc, &rc, hBrush);
    ReleaseDC(hwnd, hdc);
    DeleteObject(hBrush);
}
LRESULT CALLBACK WndProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam) {
	ok=0;
	if(Message==WM_CREATE){
		ok=1;
		SetTimer(hwnd,ID_TIM,0,Timers);
//		SetTimer(hwnd, ID_TIMER, 1000, TimerProc);
		info=CreateWindow("Static",NULL,SS_RIGHT|WS_BORDER|WS_CHILD|WS_VISIBLE,5,90,160,30,hwnd,NULL,hin,0);
		SetWindowText(info,"小时：分钟： 秒：毫秒");
		thour=CreateWindow("Static",NULL,SS_RIGHT|WS_BORDER|WS_CHILD|WS_VISIBLE,5,10,75,75,hwnd,NULL,hin,0);
		mint=CreateWindow("Static",NULL,SS_RIGHT|WS_BORDER|WS_CHILD|WS_VISIBLE,95,10,75,75,hwnd,NULL,hin,0);
		sced=CreateWindow("Static",NULL,SS_RIGHT|WS_BORDER|WS_CHILD|WS_VISIBLE,185,10,75,75,hwnd,NULL,hin,0);
		usce=CreateWindow("Static",NULL,SS_RIGHT|WS_BORDER|WS_CHILD|WS_VISIBLE,270,10,75,75,hwnd,NULL,hin,0);
		stop=CreateWindow("Button","暂停",BS_PUSHBUTTON|WS_CHILD|WS_VISIBLE,170,100,40,40,hwnd,(HMENU)ID_stop,hin,0);
		cont=CreateWindow("Button","继续",BS_PUSHBUTTON|WS_CHILD|WS_VISIBLE,215,100,40,40,hwnd,(HMENU)ID_con,hin,0);
		reset=CreateWindow("Button","重置",BS_PUSHBUTTON|WS_CHILD|WS_VISIBLE,260,100,40,40,hwnd,(HMENU)ID_reset,hin,0);
		sta=CreateWindow("Button","开始",BS_PUSHBUTTON|WS_CHILD|WS_VISIBLE,305,100,40,40,hwnd,(HMENU)ID_sta,hin,0);
		close=CreateWindow("Button","关闭",BS_PUSHBUTTON|WS_CHILD|WS_VISIBLE,5,120,160,30,hwnd,(HMENU)ID_close,hin,0);
		clear();
    	HBRUSH          hBrush;
    	HDC             hdc;
    	RECT            rc;
    	MessageBeep(-1);
	    GetClientRect(hwnd, &rc);
    	hdc     = GetDC(hwnd);
		hBrush = CreateSolidBrush(RGB(rand()%255, rand()%255, rand()%255));
  	    FillRect(hdc, &rc, hBrush);
  	    ReleaseDC(hwnd, hdc);
 	    DeleteObject(hBrush);
	}
	else if(Message==WM_COMMAND){
		ok=1;
		switch(wParam){
				case ID_reset:{
    	HBRUSH          hBrush;
    	HDC             hdc;
    	RECT            rc;
    	MessageBeep(-1);
	    GetClientRect(hwnd, &rc);
    	hdc     = GetDC(hwnd);
		hBrush = CreateSolidBrush(RGB(rand()%255, rand()%255, rand()%255));
  	    FillBackGround(hdc, &rc, hBrush);
  	    ReleaseDC(hwnd, hdc);
 	    DeleteObject(hBrush);
					sit=0;
					clear();
					break;
				}
				case ID_sta:{
					sit=1;
					break;
				}
				case ID_stop:{
					if(sit==1)sit=2;
					break;
				}
				case ID_con:{
					if(sit==2) sit=1;
					break;
				}
				case ID_close:{
				sit=0;
				int now=MessageBox(hwnd,"是否关闭","提示",MB_OKCANCEL);
				if(now==IDOK){DestroyWindow(hwnd);KillTimer(hwnd,ID_TIM);}break;}
		}
	}
	else if(Message==WM_DESTROY){
		ok=1;
		KillTimer(hwnd,ID_TIM);
		KillTimer(hwnd, ID_TIMER);
		PostQuitMessage(0);
	}
	if(!ok)return DefWindowProc(hwnd, Message, wParam, lParam);
	return 0;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                    PSTR szCmdLine, int iCmdShow)
{
    static TCHAR szAppName[] = TEXT("clock");
    HWND         hwnd;
    MSG          msg;
    WNDCLASS     wndclass;

    wndclass.style         = CS_HREDRAW | CS_VREDRAW;
    wndclass.lpfnWndProc   = WndProc;
    wndclass.cbClsExtra    = 0;
    wndclass.cbWndExtra    = 0;
    wndclass.hInstance     = hInstance;
    wndclass.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
    wndclass.hCursor       = LoadCursor(NULL, IDC_ARROW);
    wndclass.hbrBackground = (HBRUSH)GetStockObject (WHITE_BRUSH);
    wndclass.lpszMenuName  = NULL;
    wndclass.lpszClassName = szAppName;

    if (!RegisterClass (&wndclass))
    {
    MessageBox(NULL, TEXT("This program requires Windows NT!"), szAppName, MB_ICONERROR);
    return 0 ;
    }

    hwnd = CreateWindow(szAppName, TEXT ("Beeper2 Timer Demo"),
                        WS_OVERLAPPEDWINDOW,
                        CW_USEDEFAULT, CW_USEDEFAULT,
                        CW_USEDEFAULT, CW_USEDEFAULT,
                        NULL, NULL, hInstance, NULL);

    ShowWindow(hwnd, iCmdShow);
    UpdateWindow(hwnd);

    while (GetMessage(&msg, NULL, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
    return msg.wParam;
}
