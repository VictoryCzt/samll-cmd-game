#ifndef CREATER
#define CREATER

#include<iostream>
#include<ctime>
#include<cmath>
#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<algorithm>
#include<string>
#include<vector>
#include<map>
#include<set>
#include<queue>
#include<stack>
/*****header*/
#define ll long long
#define db double
#define ull unsigned long long

using namespace std;

const db infd=1e200;
const ll infl=1ll<<61;
const int infi=0x7fffffff;
const char *Error="Error,Number is too large!\n";

ll abs(ll a){return a<0?-a:a;}
ll rev(ll a){return -a;}

/**************基础*****************/
void init(){srand(time(NULL));}

ll randInt(ll down=-infl,ll up=infl){
	if(down>up) return 0;
	ll now=-infl;
	while(now<=down||now>up){
		now=(rand()*1000+rand()*100+rand()*10+rand()+rand()*1000+rand()/10);
		now=now%up+down;
		now+=rand();
		now%=up;
		int kl=rand()%100;
		if(down<0&&kl<=50) now=-now;
	}
	return now;
}
int randSInt(int down=-infi,int up=infi){
	if(down>up) return 0;
	ll now=down-1;++up;
	while(now<down||now>up){
		now=(rand()+rand())%up;
	}
	return now;
}
char randChar(int down=0,int up=355){
	if(down>up) return 0;
	char tc=-1;
	while(tc<down||tc>up){tc=(rand()%up+down)%up;}
	return tc;
}

void PutChar(char a=rand()%355){cout<<a;}
void PutLowLetter(char a='a'+rand()%26){cout<<a;}
void PutUpLetter(char a='A'+rand()%26){cout<<a;}
void PutLetter(char a=(rand()%100>50?'A':'a')+rand()%26){cout<<a;}
template <class T>void PutInt(T a=(T)rand()){cout<<a;}

/*************数列*****************/

void PutLine(ll down=-infl+1ll,ll up=infl,ll sze=1ll)
{while(sze--){cout<<randInt(down-1ll,up)<<" ";}}

void PutIncLine(ll down=-infl+1ll,ll up=infl,ll sze=1ll,ll space=1ll)
{ll now=randInt(down-1,up);while(sze--)cout<<now+space<<" ",now+=space;}
void PutRIncLine(ll down=-infl+1ll,ll up=infl,ll sze=1ll){
 	ll now=randInt(down-1ll,up);
 	ll inc=randInt(0,(up-now)/sze);
 	while(sze--)cout<<now+inc<<" ",now+=inc;}
void PutSIncLine(ll down=0,ll range=1ll,ll sze=1ll){
	ll now=randInt();
	while(sze--){
		ll ad=randInt(down-1,range);
		now+=ad;
		cout<<now<<" ";
	}
}

void PutDrLine(ll down=-infl+1ll,ll up=infl,ll sze=1ll,ll dr=1ll){
	ll now=randInt(down-1,up);
	while(sze--)cout<<now-dr<<" ",now-=dr;
}
void PutRDrLine(ll down=-infl+1ll,ll up=infl,ll sze=1ll){
	ll now=randInt(down-1,up);
	ll dr=randInt(0,(now-down)/sze);
	while(sze--)cout<<now-dr<<" ",now-=dr;
}
void PutSDrLine(ll down=0,ll range=1ll,ll sze=1ll){
	ll now=randInt();
	while(sze--){
		ll ad=randInt(down-1,range);
		now-=ad;
		cout<<now<<" ";
	}
}

void PutSameLine(ll down=-infl-1ll,ll up=infl,ll same=1,ll diff=0,ll sze=1ll){
	ll now=same,dif=diff;
	while(sze){
		ll num=randInt(down-1,up);
		while(now--) cout<<num<<" ",sze--;
		while(dif--) cout<<randInt(down-1,up)<<" ",sze--;
		now=same,dif=diff;
	}
}

void PutFebonaci(int begin=1,int end=10){
	if(end>50) {
		cout<<Error;
	}
	ll now=1ll,last=1ll,lastone=0ll;
	for(int i=1;i<=begin;i++) now=last+lastone,lastone=last,last=now;
	for(int i=begin;i<=end;i++) cout<<now<<" ",now=last+lastone,lastone=last,last=now;
}

ull ctl[34]={0,1};
bool flag;
void calc(){
	int i;
	for(i=2;i<34;i++)
	ctl[i]=ctl[i-1]*(4*i-2)/(i+1);
}
void PutKatelan(int a){
	if(!flag) flag=1,calc();
	if(a>=34) cout<<Error;
	else cout<<ctl[a]<<" ";
}


/*************字符串***************/

char *Str(int down=0,int up=355,int sze=100){
	int len=rand()%sze+1;
	char *str=new char[len];
	for(int i=0;i<len;i++) str[i]=randChar(down,up);
	return str;
}

void PutStr(int down=0,int up=355,int sze=1)
{while(sze--){cout<<randChar(down,up);}}

void PutStrWithSpace(int down=0,int up=355,int sze=1,int space=1)
{if(down>up) return ;int cnt=0;while(sze--){cnt++;cout<<randChar(down,up);if(cnt==space)cout<<" ";}}

void PutPaliStr(int down=0,int up=335,int r=1,bool odd=0,char end=0){
	char *str= new char[(r<<1|1)+odd];
	for(int i=0;i<r;i++) str[i]=randChar(down,up);
	if(odd) str[r+odd]=randChar(down,up);
	for(int i=0;i<r;i++) cout<<str[i];
	if(odd) cout<<str[r+odd];
	for(int i=r-1;i>=0;i--) cout<<str[i];
	if(end!=0)
	cout<<end;
}
void PutPaliStr2(int down=0,int up=335,int r=1,bool odd=0){
	char *str= new char[(r<<1|1)+odd];
	for(int i=0;i<r;i++) str[i]=randChar(down,up);
	if(odd) str[r+odd]=randChar(down,up);
	for(int i=0;i<r;i++) cout<<str[i];
	if(odd) cout<<str[r+odd];
	for(int i=r-1;i>=0;i--) cout<<str[i];
	delete [] str;
}
void PutSamePaliStr(int down=0,int up=335,int r=1,bool odd=0,int same=1){
	char *str= new char[(r<<1|1)+odd];
	str[0]=randChar(down,up);
	for(int i=1;i<r;i++){
		int kl=rand()%100;
		if((kl>60&&same)||(r-i<=same)){
			str[i]=str[i-1];
		}
	}
}
//Palindrome string回文串

void PutDefStr(char *str,ll sze=1ll,char end=0){
	while(sze--) cout<<str<<end;
}
void PutDefStr(char *str,ll sze=1ll){
	while(sze--) cout<<str;
}


/************图论******************/

int randNot(int a=-infi,int down=-infi,int up=infi-1){
	if(down>up) return 0;
	int now=a;++up;
	while(now==a||now<down||now>up){
		now=(rand()+rand())%up;
	}
	return now;
}
template <class T>
bool dcmp(T a,T b){return rand()<rand();}
void PutTree(int sze=5,int sta=1,int Type=0,bool ishuffle=0,ll downv=0,ll upv=100){
	printf("%d\n",sze);
	if(Type==1||Type==3){
		for(int i=1;i<=sze;i++) printf("%lld ",randInt(downv-1,upv));printf("\n");
	}
	int *array =new int[sze+1];
	for(int i=1;i<=sze;i++) array[i]=i-1;
	if(ishuffle)random_shuffle(array+1,array+sze+1);
	for(int i=sta,j=1;j<sze;i++,j++){
		int t1=array[i],t2=randNot(array[i],0,sze-1);
		int mod=array[i];if(!mod)mod+=sze;
		while(t2==t1) t2=randNot(array[i],0,sze-1)%mod;
		int kl=rand()%100;
		if(kl<50) swap(t1,t2);
		printf("%d %d",t1+sta,t2+sta);
		if(Type==2||Type==3){
			printf(" %lld\n",randInt(downv-1,upv));
		}else printf("\n");
	}
}
void PutGra(int sze=5,int sta=1,int side=10,int Type=0,ll downv=0,ll upv=100){
	printf("%d %d\n",sze,side);
	if(Type==1||Type==3){
		for(int i=1;i<=sze;i++) printf("%lld ",randInt(downv-1,upv));printf("\n");
	}
	for(int i=sta;i<=sze+sta-1&&side;i++,side--){
		int t1=i,t2=randNot(t1,sta,sze+sta-1);
		int kl=rand()%100;
		if(kl<50) swap(t1,t2);
		printf("%d %d",t1,t2);
		if(Type==2||Type==3){
			printf(" %lld\n",randInt(downv-1,upv));
		}else printf("\n");
	}
	while(side--){
		printf("%d %d",randSInt(sta,sze+sta-1),randSInt(sta,sze+sta-1));
		if(Type==2){
			printf(" %lld\n",randInt(downv-1,upv));
		}else printf("\n");
	}
}
void PutChain(int sze=5,int sta=1,int Type=0,ll downv=0,ll upv=100,bool isside=0){
	printf("%d",sze);
	if(isside) printf(" %d\n",sze-1);
	else printf("\n");
	if(Type==1||Type==3){
		for(int i=1;i<=sze;i++) printf("%lld ",randInt(downv-1,upv));printf("\n");
	}
	for(int i=sta;i<sze+sta-1;i++){
		printf("%d %d",i,i+1);
		if(Type==2||Type==3){
			printf(" %lld\n",randInt(downv-1,upv));
		}else printf("\n");
	}
}

void PutFlower(int sze=5,int mid=1,int Type=0,ll downv=0,ll upv=100,bool isside=0){
	printf("%d",sze);
	if(isside) printf(" %d\n",sze-1);
	else printf("\n");
	if(Type==1||Type==3){
		for(int i=1;i<=sze;i++) printf("%lld ",randInt(downv-1,upv));printf("\n");
	}
	for(int i=1;i<sze;i++){
		if(i==mid) continue;
		int t1=i,t2=mid;
		int kl=rand()%100;
		if(kl<50) swap(t1,t2);
		printf("%d %d",t1,t2);
		if(Type==2||Type==3){
			printf(" %lld\n",randInt(downv-1,upv));
		}else printf("\n");
	}
}

void PutKthTree(int sze=10,bool isfrom0=0,int Type=0,int kth=2,bool ishuffle=0,ll downv=0,ll upv=100){
	printf("%d\n",sze);
	if(Type==1||Type==3){
		for(int i=1;i<=sze;i++) printf("%lld ",randInt(downv-1,upv));printf("\n");
	}
	int *array =new int[sze+1];
	for(int i=1;i<=sze;i++) array[i]=i-isfrom0;
	if(ishuffle)random_shuffle(array+1,array+sze+1);
	for(int i=1;i<=sze;){
		for(int j=1;j<=kth&&i+j<=sze;j++){
			printf("%d %d",array[i],array[i+j]);
			if(Type==2||Type==3){
				printf(" %lld\n",randInt(downv-1,upv));
			}printf("\n");
		}
		i+=kth;
	}
}

/************几何&浮点数***********/

struct point{
	db x,y;
	point(db x=0,db y=0):x(x),y(y){}
	void in(){scanf("%lf%lf",&x,&y);}
	void out2(){printf("%.2lf %.2lf\n",x,y);}
	void out6(){printf("%lf %lf\n",x,y);}
	void out10(){printf("%.10lf %.10lf\n",x,y);}
	void out0(){printf("%.0lf %.0lf\n",x,y);}
};

void PutSquare(db zx=0,db zy=0,db len=1,bool inv=0,int pot=2){
	if(len<0||pot<0){
		cout<<Error;return;
	}
	point t1=point(zx,zy);
	point t2=point(zx+len,zy);
	point t3=point(zx+len,zy+len);
	point t4=point(zx,zy+len);
	if(inv){
		if(pot==0){
			t1.out0();t4.out0();t3.out0();t2.out0();
		}else if(pot>=1&&pot<=2){
			t1.out2();t4.out2();t3.out2();t2.out2();
		}else if(pot>2&&pot<=6){
			t1.out6();t4.out6();t3.out6();t2.out6();
		}else if(pot>6){
			t1.out10();t4.out10();t3.out10();t2.out10();
		}
	}else{
		if(pot==0){
			t1.out0();t2.out0();t3.out0();t4.out0();
		}else if(pot>=1&&pot<=2){
			t1.out2();t2.out2();t3.out2();t4.out2();
		}else if(pot>2&&pot<=6){
			t1.out6();t2.out6();t3.out6();t4.out6();
		}else if(pot>6){
			t1.out10();t2.out10();t3.out10();t4.out10();
		}
	}
}

/*********高精数*****************/
int randSig(int tt=-1,int up=10){
	int now=-1;
	while(now<0||now>up||now==tt){now=(rand()+rand())%up;}
	return now;
}
void PutBigInt(int bit=10,int hex=10,bool pre_zero=0){
	if(!pre_zero)bit--,cout<<randSig(0,hex);
	while(bit--){cout<<randSig(-1,hex);}
	cout<<endl;
}
void PutBigInvInt(int bit=10,int hex=10,bool pre_zero=0){
	cout<<"-";
	if(!pre_zero)bit--,cout<<randSig(0,hex);
	while(bit--){cout<<randSig(-1,hex);}
	cout<<endl;
}
void PutFloat(int bit=1,int pot=5,int pd=1){
	if(pd==-1) printf("-");
	bit--;cout<<randSig(0);
	while(bit--) cout<<randSig();
	cout<<".";
	while(pot--) cout<<randSig();
}

/************特殊****************/



#endif
