#include<cstdio>
#include<cstring>
#include<algorithm>

using namespace std;

namespace Vector{
template <class T>
	void swap(T &a,T &b){T t=a;a=b;b=t;}
template <class T>
	struct vector{
		typedef T* iterator;
	private:
		T *v;
		int sze,newsze;
	public:
		vector(int s=1001){
			v= new T[s];sze=0;
			newsze=s;
		}
		void require(){
			if(sze<newsze) return;
			T *now=v;
			newsze=newsze<<1|1;
			v =new T[newsze];
			for(int i=0;i<=sze;i++)
			v[i]=now[i];
			delete[] now;
		}
		void push_back(T a){
			require();
			v[++sze]=a;
		}
		void erase(int num){
			for(int i=num;i<sze-1;i++){
				swap(v[i],v[i+1]);
			}
			sze--;v[sze]=0;
		}
		void clear(){
			delete v;
			v= new T[1001];
			newsze=1001;
		}
		int size(){return sze;}
		void pop_back(){
			if(!sze) return;
			--sze;
		}
		iterator begin(){return &v[0];}
		iterator end(){return &v[size()];}
		bool empty(){
			return sze==0;
		}
		T &operator [](int a){
			if(a>sze) return -0x7fffffff;
			return v[a];
		}
		vector &operator =(const vector& a){
			if(this==a) return *this;
			delete [] v;
			sze=a.sze;
			newsze=a.newsze;
			v= new T[newsze];
			for(int i=0;i<sze;++i)
			v[i]=a.v[i];
			return *this;
		}
	};
}
using namespace Vector;

