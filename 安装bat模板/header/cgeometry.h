#include<bits/stdc++.h>
#define db double
using namespace std;
//计算几何常用模板 
namespace Vector{
   //定义点和向量 
   struct Point{
      db x,y;
      Point(db a=0,db b=0):x(a),y(b){}
   };
   #define p Point
   //输入点
   void pin(p &a){scanf("%lf%lf",&a.x,&a.y);}  
   //向量+向量=向量，点+向量=向量; 
   p operator +(p a,p b){return p(a.x+b.x,a.y+b.y);}
   //点-点=向量
   p operator -(p a,p b){return p(a.x-b.x,a.y-b.y);}
   //向量*数=向量
   p operator *(p a,db b){return p(a.x*b,a.y*b);}
   //向量/数=向量
   p operator /(p a,db b){return p(a.x/b,a.y/b);}
   //比较
   bool operator <(p a,p b){return a.x<b.x||(a.x==b.x&&a.y<b.y);}
   const db eps=1e-10;
   int cmp(db x){if(fabs(x)<eps)return 0;return x<0?-1:1;}
   bool operator ==(p a,p b){return cmp(a.x-b.x)==0&&cmp(a.y-b.y)==0;}
   //点积，长度，角度（弧度制,夹角）
   db dot(p a,p b){return a.x*b.x+a.y*b.y;}
   db length(p a){return sqrt(dot(a,a));}
   db angle(p a,p b){return acos(dot(a,b)/length(a)/length(b));}
   //叉积，左右位置判断
   db cross(p a,p b){return a.x*b.y-a.y*b.x;}
   db area(p a,p b,p c){return cross(b-a,c-a);}
   //向量旋转弧度制x=xcosa-ysina;y=xsina+ycosa;(a为度数);
   p rotate(p a,db rad){return p(a.x*cos(rad)-a.y*sin(rad),a.x*sin(rad)+a.y*cos(rad));}
   //单位法线，即左转90度，长度归一化
   p normal(p a){db l=length(a);return p(-a.y/l,-a.x/l);} 
}
using namespace Vector;
namespace Line{
   //定义线段 
   struct seg_line{
   	p st,en;
   };
   //判断两线是否规范相交0相交1不相交 
   bool operator | (seg_line a,seg_line b){
   	db p1=area(a.st,a.en,b.st),p2=area(a.st,a.en,b.en);
   	if(p1*p2>=0) return 1;
   	p1=area(b.st,b.en,a.st),p2=area(b.st,b.en,a.en);
   	if(p1*p2>=0) return 1;
   	return 0;
   }
   //输入一条线段
   void linein(seg_line &a){
   	pin(a.st);pin(a.en); 
   } 
}

namespace Calc{
   //一些简易计算工具 
   db distan(p a,p b){
   	return sqrt((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y));
   }
}
