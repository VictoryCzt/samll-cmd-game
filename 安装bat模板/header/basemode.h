#ifndef _BASEMODE_H
#define _BASEMODE_H
#include<bits/stdc++.h>
#define ll long long
#define ull unsigned long long
#define db double
using namespace std;
const int M=2e6+10;
//这是一些常用模板 
const int inf=0x7fffffff;
namespace Base{
	 int read()
	{
    int X=0,w=1; char ch=0;
    while(ch<'0' || ch>'9') {if(ch=='-') w=-1;ch=getchar();}
    while(ch>='0' && ch<='9') X=(X<<3)+(X<<1)+ch-'0',ch=getchar();
    return X*w;
	}
	 void write(int x)
	{
     if(x<0) putchar('-'),x=-x;
     if(x>9) write(x/10);
     putchar(x%10+'0');
	}
    void outtime(){
    	printf("\nTime used = %.5lf\n",(double)clock()/CLOCKS_PER_SEC);
    }
    long long fact(long long a){return a==1?1:fact(a-1)*a;}
	long long Quickpower(long long a,long long b){
		if(b==1) return a;
		long long t=Quickpower(a,b/2);
		t=t*t;if(b%2) t=t*a;
		return t;
	}
	long long Guickpower(long long a,long long b){
		long long t=a;
		for(;b;b>>=1,t*=t){
			if(b%2) a*=t;
		}
		return a;
	}
	void Exgcd(int a,int b,int &x,int &y){
		if(!b){x=1,y=0;}
		else {Exgcd(b,a%b,y,x);y-=x*(a/b);}
	}//逆元为x 
	bool PSquare(long long a){
		long long t=(long long)sqrt(a);
		if(t*t==a) return 1;return 0;
	} 
	int max(int a,int b){return a>b?a:b;}
    int min(int a,int b){return a>b?b:a;}
    void change(int &a,int &b){int t=a;a=b;b=t;}
    void exchange(int &a,int &b){a^=b^=a^=b;}
    struct QsortA{
    	int n,x[M];
    	void init(){memset(x,0,sizeof(x));n=0;}
		void Qsort(int a,int b){
	    if(a==b) return;
	    int val=x[(a+b)>>1];
	    int i=a-1,j=b+1;
	    while(1){
		do i++;while(x[i]<val);
		do j--;while(x[j]>val);
		if(j<=i) break;
		change(x[i],x[j]);
	    }
	    Qsort(a,j);
	    Qsort(j+1,b); 
        }
        void out(){
        	for(int i=1;i<=n;i++) printf("%d ",x[i]);
        }
        void in(){
        	for(int i=1;i<=n;i++) scanf("%d",&x[i]);
        }
    };
    struct GsortA{
    	int n,td[M],data[M];
    	void init(){memset(td,0,sizeof(td));memset(data,0,sizeof(data));n=0;}
        void Gsort(int a,int b)
    	{
		if(a==b) return;
		int mid=(a+b)/2;
	 	Gsort(a,mid);
	 	Gsort(mid+1,b);
	 	int f=a,f1=a,f2=mid+1;
	 	while(f1<=mid&&f2<=b)
	 	{
			if(data[f1]<data[f2])
			 td[f++]=data[f1++];
			else
			 td[f++]=data[f2++];
		 }
		 for(int i=f1;i<=mid;i++)
		 td[f++]=data[i];
		 for(int i=f2;i<=b;i++)
	 	td[f++]=data[i];
     	for(int i=a;i<=b;i++)
     	data[i]=td[i]; 
    	}
    	void out(){
    		for(int i=1;i<=n;i++) printf("%d ",data[i]);
    	}
    	void in(){
    		for(int i=1;i<=n;i++) scanf("%d",&data[i]);
    	}
    };
    int Gcd(int a,int b){
    	if(!b) return a;
    	return Gcd(b,a%b);
    }
template <class T>
  struct loop_array{
   	T a[M];
   	int n,st;
   	loop_array(){n=st=0;}
   	T operator [](int x){if(x>=n) x%=n;if(x<0){
   		while(x<0)x+=n;
	   }return a[x];}
   	void insert(T b){a[n++]=b;}
   	T begin(){return a[st];}
   	T end(){return a[n-1];}
   	int size(){return n;}
};
}
using namespace Base;
namespace Graph_theory{
    struct LCA
    {	
	int n,m,root,dep[M],jump[M][22],head[M],p;
    int to[M],last[M];
    void add(int a,int b){
       to[++p]=b;last[p]=head[a];head[a]=p;
       to[++p]=a;last[p]=head[b];head[b]=p;
	}
	void dfs(int a)
	{
	for(int i=1;i<=21;i++)
	jump[a][i]=jump[jump[a][i-1]][i-1];
	for(int i=head[a];i;i=last[i])
		if(to[i]!=jump[a][0])
		{
			dep[to[i]]=dep[a]+1;
			jump[to[i]][0]=a;
			dfs(to[i]);
		}	
	}
	int lca(int a,int b)
	{
	if(dep[a]<dep[b]) change(a,b);
	for(int i=21;i>=0;i--)
	if(dep[jump[a][i]]>=dep[b])
	a=jump[a][i];
	if(a==b) return b;
	for(int i=21;i>=0;i--)
	if(jump[a][i]!=jump[b][i])
	{
		a=jump[a][i];
		b=jump[b][i];
	}
	return jump[b][0];
	}
	};
	const int maxn=1010,maxm=300010;
	struct Maxflow_Mincost{
	int head[maxn],cap[maxm*2],p,last[maxm*2],to[maxm*2],cost[maxm*2];
	int s,t,n,maxflow,mincost,dis[maxn],from[maxn],way[maxn],line[maxn],used[maxn];
	void init()
	{
		p=1;
	   memset(head,0,sizeof(head));memset(cap,0,sizeof(cap));
	} 
	void add(int a,int b,int c,int d)
	{
		last[++p]=head[a];head[a]=p;to[p]=b;cap[p]=c;cost[p]=d;
		last[++p]=head[b];head[b]=p;to[p]=a;cap[p]=0;cost[p]=-d;
	}
	bool SPFA()
	{
		for(int i=1;i<=n;i++)
		dis[i]=inf;
		dis[s]=0;
		int st,en;
		line[st=en=0]=s;
		while(st<=en)
		{
			int tt=line[st%n]; st++;
			used[tt]=0;
			for(int i=head[tt];i;i=last[i])
			if(cap[i]&&dis[to[i]]>dis[tt]+cost[i])
			{
				int kk=to[i];
				dis[kk]=dis[tt]+cost[i];
				way[kk]=i;from[kk]=tt;
				if(!used[kk])
				{
					used[kk]=1;
					en++;
					line[en%n]=kk;
				}
			}
		}
		return dis[t]<inf;
	}
	void work()
	{
		maxflow=mincost=0;
		while(SPFA())
		{
			int minflow=inf;
			for(int i=t;i!=s;i=from[i])
			   minflow=min(minflow,cap[way[i]]);	
			   maxflow+=minflow;
			for(int i=t;i!=s;i=from[i])
			   {
			   	mincost+=cost[way[i]]*minflow;
			   	cap[way[i]]-=minflow;
			   	cap[way[i]^1]+=minflow;
		       }
		}
	}	
   };
//最小费模板使用样例 
//using namespace MFMC;
//int N,M;
//int main()
//{
//	scanf("%d%d",&N,&M);
//	s=1;t=N;n=N;
//	int A,B,C,D;
//	for(int i=1;i<=M;i++)
//	{
//		scanf("%d%d%d%d",&A,&B,&C,&D);
//		add(A,B,C,D);
//	}
//	work();
//	printf("%d %d\n",maxflow,mincost);
//	return 0;
//}
    struct Kruskal{
        int fa[M],ans[M],n,m,tot,cnt;
        int find(int a){return fa[a]==a?a:fa[a]=find(fa[a]);}
		struct Edge{
        	int from,to,len;
        	bool operator <(Edge a)const{
			  return len<a.len;
			} 
        }g[M];
        void init(){memset(fa,0,sizeof(fa));memset(ans,0,sizeof(ans));memset(g,0,sizeof(g));}
        void mintree(){
        	tot=0;cnt=0;
        	sort(g+1,g+n+1);
        	for(int i=1;i<=n;i++) fa[i]=i;
        	for(int i=1;i<=m;i++) {
        		int x=find(g[i].from),y=find(g[i].to);
        		if(x!=y){
        		   tot+=g[i].len;
				   ans[++cnt]=i;
				   fa[x]=y;	
        		}
        		if(cnt==n-1) break;
        	}
        }
        void outmintree(){
        	for(int i=1;i<=cnt;i++){
        		printf("%d %d\n",g[ans[i]].from,g[ans[i]].to);
        	}
        }
	};
	struct SPFA{
		int n,m,s,head[M],l[M],p,q,dis[M],pos;
		bool used[M];
		struct ss
		{
			int to,last,dist;
		}g[M*2];
		void init(){memset(used,0,sizeof(used));memset(g,0,sizeof(g));memset(head,0,sizeof(head));
		            memset(dis,0,sizeof(dis));
		}
		void change(int &a,int &b){int t=a;a=b;b=t;}
		void add(int a,int b,int c){
		g[++pos]=(ss){b,head[a],c};head[a]=pos;
		}
		void read()
		{
		scanf("%d%d%d",&n,&m,&s);
		int t1,t2,t3;
		for(int i=1;i<=m;i++)
		scanf("%d%d%d",&t1,&t2,&t3),add(t1,t2,t3);
		}
		void spfa()
		{
		for(int i=1;i<=n;i++)
		dis[i]=inf;
		dis[s]=0,l[0]=s,used[s]=1;
		for(;p<=q;p++)
		{
		int now=head[l[p%n]];
		for(;now;now=g[now].last)
		if(dis[g[now].to]>dis[l[p%n]]+g[now].dist)
		{
		  dis[g[now].to]=dis[l[p%n]]+g[now].dist;
		  if(!used[g[now].to])
		  {
		  	l[++q%n]=g[now].to;
		  	used[g[now].to]=1;
		  	if(dis[l[(p+1)%n]]>dis[l[q%n]])
		  	change(l[(p+1)%n],l[q%n]);
		  }	
		}
		used[l[p%n]]=0;
		}
		} 
	};
	struct MaxFlow{
		int n,m,s,t,head[10010],used[10010],ok,ans,pos;
		struct ss
		{
		int to,last,flow,cap;
		}g[2*M];
		struct sss
		{
		int a,lb,ld,mi;
		}l[2*M];
		void add(int a,int b,int c){
		g[++pos]=(ss){b,head[a],0,c};head[a]=pos;
		g[++pos]=(ss){a,head[b],0,0};head[b]=pos;
		}
		void copy(int a,int b)
		{
		for(int i=a;i;i=l[i].ld)
		{
		g[l[i].lb].flow+=b;
		g[l[i].lb^1].flow-=b;
		}
		ans+=b;
		ok=1;
		}
		void bfs()
	{
	int p=0,q=0;
	l[0].a=s;used[s]=1;
	l[0].lb=l[0].ld=0;
	l[0].mi=inf;
	for(;p<=q;p++)
		for(int i=head[l[p].a];i;i=g[i].last)
			if(!used[g[i].to]&&g[i].cap-g[i].flow)
			{
				used[g[i].to]=1;
				l[++q].a=g[i].to;
				l[q].lb=i,l[q].ld=p;
				l[q].mi=min(l[p].mi,g[i].cap-g[i].flow);
				if(l[q].a==t)
				{
					copy(q,l[q].mi);
					return;
				}
			}
	}
	void read()
	{
	int a,b,c;pos=1;
	scanf("%d%d%d%d",&n,&m,&s,&t);
	for(int i=1;i<=m;i++)
	{
		scanf("%d%d%d",&a,&b,&c);
		add(a,b,c);
	}
	}
	void work()
	{
	while(1)
	{
		memset(used,0,sizeof(used));
		ok=0;
		bfs();
		if(!ok) break;
	}
//	printf("%d\n",ans);
	}
	};
	const int W=2001;
	struct Floyd{	
		int n,s,t,t1,t2,t3,way[W][W],f[W][W],ans;
		void init(){
			memset(f,0x7f,sizeof(f));
		}
		void work(){
			for(int i=1;i<=n;i++) f[i][i]=0;
			for(int k=1;k<=n;k++)
			for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
			if(i!=k&&j!=k&&i!=j)
			if(f[i][j]>f[i][k]+f[k][j]){
				f[i][j]=f[i][k]+f[k][j];way[i][j]=way[i][k];
			}
			ans=f[s][t];
		}
		void add(int a,int b,int c){
			f[a][b]=f[b][a]=min(f[a][b],c);
		}
	};
	const int N=10000+10;
	struct Dinic{
	int n,m,st,en,cur[N],dis[N],l[N],p,q,cnt;
	struct ss{
	int to,cap;
	void add(int a,int b){to=a,cap=b;}
	}g[M];
	vector <int> vec[N];
	int bfs()
	{
	p=q=1;
	memset(dis,0,sizeof(dis));
	l[1]=st,dis[st]=1;
	for(;p<=q;p++)
	{
		int v=l[p];
		for(int i=0;i<vec[v].size();i++)
		{
			int t=vec[v][i];
			if(!dis[g[t].to]&&g[t].cap)
			{
				dis[g[t].to]=dis[v]+1;
				l[++q]=g[t].to;
			}
		}
		}
		return dis[en];
	}
    int dfs(int u,int c)
    {
	if(u==en||!c) return c;
	int tot=0;
	for(int &i=cur[u];i<vec[u].size();i++){
		int now=vec[u][i];
		if(dis[u]+1==dis[g[now].to]&&g[now].cap){
			   int tnow=dfs(g[now].to,min(c,g[now].cap));
	   	g[now].cap-=tnow;
	   	g[now^1].cap+=tnow;
	   	c-=tnow;tot+=tnow;
	   	if(!c) break;	
		}
	}
	return tot;
    }
//int main()
//{
//	int ans=0;cnt=1;
//	scanf("%d%d%d%d",&n,&m,&st,&en);
//	for(int i=1;i<=m;i++)
//	{
//		int a,b,c;
//		scanf("%d%d%d",&a,&b,&c);
//		g[++cnt].add(b,c);vec[a].push_back(cnt);
//		g[++cnt].add(a,0);vec[b].push_back(cnt);
//	}
//	while(bfs())
//	{
//		memset(cur,0,sizeof(cur));
//		ans+=dfs(st,inf);
//	}
//	printf("%d\n",ans);
//	return 0;
//}
	};
}

namespace Calc{
   struct Counter{
   	int len;
    char str[1001]; 
	int gread(char a){
	if(a=='+'||a=='-')
	return 1;
	if(a=='*'||a=='/')
	return 2;
	return 3;
	}	 
	int gn(int a,int b){
	int t=0;
	for(int i=a;i<=b;i++)
	t=t*10+str[i]-48;
	return t;
	}
	int isfh(char a){
	if(a=='+'||a=='-'||a=='*'||a=='/')
	return 1;
	return 0;
	}
	int calc(int a,int b){
		char best='#';
		int pos,cc=0;
		for(int i=b;i>=a;i--)
		{
		if(str[i]==')')
		{
		cc++;
		continue;		
		}
	    if(str[i]=='(')
		{
		cc--;
		continue;		
		}
		if(isfh(str[i])==1)
		{
		if(gread(best)>gread(str[i])&&cc==0)
		{
		best=str[i];
		pos=i;
		}
		}
	    }
		if(best=='#')
		{
			if(str[a]=='(')
			return calc(a+1,b-1);	
			return gn(a,b);
		}
		switch(best)
		{
			case '+': return calc(a,pos-1)+calc(pos+1,b);
			case '-': return calc(a,pos-1)-calc(pos+1,b);
			case '*': return calc(a,pos-1)*calc(pos+1,b);
			case '/': return calc(a,pos-1)/calc(pos+1,b);
		}
	}
   };
   int ok=1;
    struct HighPlus{
    	
        struct ln{
	    long long len,d[1005];
	    char str[1005];
	    ln(char a[]){
		len=strlen(a);
		for(int i=0;i<len;i++)
		d[len-i]=a[i]-48;
	}
	//字符转数字；
	ln(){
		//len=1;d[1]=0;
    } 
	ln operator +(ln b){
		ln t;
		if(len</*=*/b.len)
		for(int i=len+1;i<=b.len;i++)
		d[i]=0;
		if(b.len<len)
		for(int i=b.len+1;i<=len;i++)
		b.d[i]=0;
		//差位补0；
		int l=len;
		if(l<b.len) l=b.len;
		int w=0;
		for(int i=1;i<=l;i++){
			t.d[i]=d[i]+b.d[i]+w;
			w=t.d[i]/10;
			t.d[i]%=10;
		}//计算 
		while(w){
			t.d[++l]=w%10;
			w/=10;
		} 
		t.len=l;
		return t;//进位 
	}
	void out(){
//		while(!d[i]) len--;
//		删前缀0方法1； 
		for(int i=len;i>=1;i--){
			if(d[i]!=0) ok=0;
			if(ok&&!d[i]) continue;
//			删前缀0方法2； 
			printf("%d",d[i]);
		}}}A[10];
//使用样例 	
//	int main()
//	{
//	while(scanf("%s",str)==1)
//	{
//		b=str;
//		a=a+b;
//	}
//	a.out();
//	return 0;
//	}
//使用样例 
//int main()
//{
//	scanf("%s",&str[1]);
//	len=strlen(&str[1]);
//	printf("%d\n",calc(1,len));
//	return 0;
//}
   };const int WW=210;
   struct Matrix{
   	 int m[WW][WW],h,l;
   	 void clear(){memset(m,0,sizeof(0));h=l=0;}
   	 Matrix operator *(Matrix a){
   	 	Matrix t;
   	 	t.h=h,t.l=a.l;
		for(int i=1;i<=h;i++)
   	 	for(int j=1;j<=a.l;j++){
   	 		t.m[i][j]=0;
   	 		for(int k=1;k<=l;k++)
   	 		t.m[i][j]+=(m[i][k]*a.m[k][j]);
   	 	}
   	 	return t;
   	 }
   	 Matrix operator +(Matrix a){
   	 	Matrix t;
   	 	t.h=h,t.l=a.l;
   	 	for(int i=1;i<=h;i++)
   	 	for(int j=1;j<=a.h;j++)
   	 	t.m[i][j]=m[i][j]+a.m[i][j];
   	 	return t;
   	 }
   };
}

namespace CString{
	//KMP
   struct Kmp{
   	int f[M],pos[M],cnt;
    char fa[M],son[M];
    void init(){
    	memset(fa,0,sizeof(fa));
    	memset(son,0,sizeof(son));
    	memset(f,0,sizeof(f));
    	memset(pos,0,sizeof(pos));cnt=0;
    }
    void getfail(char* a,int* f)
    {
	int l1=strlen(a);
	f[1]=f[0]=0;
	for(int i=1;i<=l1;i++)
	{
		int j=f[i];
		while(j&&a[i]!=a[j]) j=f[j];
		f[i+1]=a[i]==a[j]?j+1:0;
	}
    }
    void kmp(char* a,char* b,int* f)
    {
	int l1=strlen(a),l2=strlen(b);
	getfail(b,f);
	int j=0;
	for(int i=0;i<l1;i++)
	{
		while(j&&a[i]!=b[j]) j=f[j];
		if(a[i]==b[j]) j++;
		if(j==l2) pos[++cnt]=i-l2+2;
	}
	 return;
    }
   };
   struct Exkmp{
   	  int ex[M],nex[M],len1,len2;
   	  char s1[M],s2[M];
   	void init(){
   		memset(ex,0,sizeof(ex));memset(nex,0,sizeof(nex));len1=len2=0;
   		memset(s1,0,sizeof(s1));memset(s2,0,sizeof(s2));
   	}
	int getnex(char *a){
	int l1=strlen(a),j=0,k=1;
	nex[0]=l1;
	for(;j+1<l1&&a[j]==a[j+1];j++);
	nex[1]=j;
	for(int i=2;i<l1;i++){
	    int p=nex[k]+k,l=nex[i-k];
		if(p>l+i) nex[i]=l;
		else {
			j=max(0,p-i);
			for(;j+i<l1&&j<l1&&a[i+j]==a[j];j++);
			nex[i]=j;k=i;
		}	
	}
	return l1;
    }
    
	void exkmp(char* a,char* b){
	int l1=strlen(a),l2=getnex(b);
	int j=0,k=0;len1=l1,len2=l2;
	for(;j<l1&&j<l2&&a[j]==b[j];j++);
	ex[0]=j;
	for(int i=1;i<l1;i++){
		int p=ex[k]+k,l=nex[i-k];
		if(p>l+i) ex[i]=l;
		else {
			j=max(0,p-i);
			for(;j+i<l1&&j<l2&&a[j+i]==b[j];j++);
			ex[i]=j;k=i;
		}
	  }
     }
   };
   //TRIE AND AUTO
   struct AC_auto{
   	  int son[M][30],fail[M],cnt[M],root,ans;
   	  int l[M],p,q;
      bool flag[M];
      char str[M];
      int idx(char a){return a-'a';}
      void init(){memset(str,0,sizeof(str));root=0;ans=0;}
      void clear(int a){memset(son[a],0,sizeof(son[a]));fail[a]=cnt[a]=flag[a]=0;}
   	  void build(char* a){
   	  	 int l1=strlen(a),now=0;
   	  	 for(int i=0;i<l1;i++){
   	  	   int id=idx(a[i]);
   	  	   if(!son[now][id]){
   	  	   	clear(++root);
			son[now][id]=root;
   	  	   }
   	  	   now=son[now][id];  
   	    }
   	    flag[now]=1;cnt[now]++;
   	  }
   	  void getfail(){
   	  	p=1,q=0;
   	  	for(int i=0;i<26;i++){
   	  		if(son[0][i]){
   	  			fail[son[0][i]]=0;
   	  			l[++q]=son[0][i];
   	  		}
   	  	}
   	  	for(;p<=q;p++){
   	  		int now=l[p];
   	  		for(int i=0;i<26;i++){
   	  			if(son[now][i]){
   	  				fail[son[now][i]]=son[fail[now]][i];
   	  				l[++q]=son[now][i];
   	  			}else
   	  			son[now][i]=son[fail[now]][i];
   	  		}
   	  	}
   	  }
   	  void Acto(char* a){
   	  	int l1=strlen(a),now=0;
   	  	for(int i=0;i<l1;i++){
   	  		int id=idx(a[i]);
			now=son[now][id];
			int j=now;
			for(;j&&flag[j];j=fail[j]){
				ans+=cnt[j];
				flag[j]=0;
			}
   	  	}
   	  }
   };
   //MANACHER 
   const int N=M>>1;
   struct Manacher{	
   	 char str1[M],str2[N];
   	 int len[N],ans;
   	 void init(){
	  memset(len,0,sizeof(len));ans=0;
     }

   int turn(){
	int l1=strlen(str1);
	int now=l1+l1;
	str2[0]='(';
	for(int i=1;i<=now;i+=2){
		str2[i]='#';
		str2[i+1]=str1[i/2];
	}
	str2[now+1]='#',str2[now+2]=')',str2[now+3]=0;
	return now+3;
   }

   void manacher(){
	init();
	int tmax=0,pos=0;
	int dlen=turn();
	for(int i=0;i<dlen;i++){
		if(tmax>i) 
		len[i]=min(tmax-i,len[pos+pos-i]);
		else len[i]=1;
		for(;str2[i+len[i]]==str2[i-len[i]];len[i]++);
		if(len[i]+i>tmax){
			tmax=len[i]+i;pos=i;
		}
		ans=max(ans,len[i]);
	}
   }

   };
   //SUFFIX:
   	struct Suffix_array{
	int len;char str[M];
	int rank[M],y[M],cnt[M],sa[M],h[M],rmq[22][M];
    void getback(){
    	memset(cnt,0,sizeof(cnt));int A=256;
    	for(int i=0;i<len;i++) cnt[rank[i]=str[i]]++;
    	for(int i=1;i<=A;i++) cnt[i]+=cnt[i-1];
    	for(int i=len-1;i>=0;i--) sa[--cnt[rank[i]]]=i;
    	for(int k=1;k<=len;k<<=1){
    		memset(cnt,0,sizeof(cnt));int p=0;
    		for(int i=len-k;i<len;i++) y[p++]=i;
    		for(int i=0;i<len;i++) if(sa[i]>=k) y[p++]=sa[i]-k;
    		for(int i=0;i<len;i++) cnt[rank[y[i]]]++;
    		for(int i=1;i<=A;i++) cnt[i]+=cnt[i-1];
			for(int i=len-1;i>=0;i--) sa[--cnt[rank[y[i]]]]=y[i];
			swap(rank,y);p=1;rank[sa[0]]=0;
			for(int i=0;i<len;i++){
				if(y[sa[i]]==y[sa[i-1]]&&y[sa[i]+k]==y[sa[i-1]+k])
				rank[sa[i]]=p-1;
				else rank[sa[i]]=p++;
			}if(p>len) break;A=p;
    	}
    	for(int i=0;i<len;i++) rank[sa[i]]=i;
    	for(int i=1,k=0;i<=len;i++){
    		if(k)k--;
    		int j=sa[rank[i]-1];
    		for(;str[j+k]==str[i+k];k++);
    		h[rank[i]]=k;
    	}
    	for(int i=0;i<len;i++) rmq[0][i]=h[i];
    	for(int i=1;(1<<i)<=len;i++)
    	for(int j=1;j+(1<<i)-1<=len;j++){
    		rmq[i][j]=min(rmq[i-1][j],rmq[i-1][j+(1<<(i-1))]);
    	}
    }
    int lcp(int l,int r){
    	if(r>len) return 0;
    	l=rank[l],r=rank[r];
    	if(l>r) swap(l,r);l++;
    	int k=0,l1=r-l+1;
    	for(;(1<<k)<=l1;++k);k--;
        return min(rmq[k][l],rmq[k][r-(1<<k)+1]);
    }
    void init(){
    	memset(str,0,sizeof(str));
    	memset(rank,0,sizeof(rank));
    	memset(rmq,0,sizeof(rmq));
    	memset(y,0,sizeof(y));len=0;
    }
    void work(){
    	len=strlen(str+1);
    	getback();
    }
	};
	struct Suffix_array_auto{
	char str[M];
	int son[M][26],fa[M],len[M],topu[M],cnt[M],n;
	int root,last;
	void init(){memset(topu,0,sizeof(topu));memset(cnt,0,sizeof(cnt));memset(str,0,sizeof(str));}
    void clear(int a){memset(son[a],0,sizeof(son[a]));fa[a]=len[a]=0;}
	void add(int a){
		int p=last,np=++root;last=np;clear(np);
		len[np]=len[p]+1;
		for(;p&&!son[p][a];p=fa[p]) son[p][a]=np;
		if(!p) fa[np]=1;
		else {
			int q=son[p][a];
			if(len[p]+1==len[q]) fa[np]=q;
			else {
				int nq=++root;clear(nq);
				len[nq]=len[p]+1;
				memcpy(son[nq],son[q],sizeof(son[q]));
				fa[nq]=fa[q];
				fa[np]=fa[q]=nq;
				for(;p&&son[p][a]==q;p=fa[p])son[p][a]=nq;
			}
		}
	}
	void topusort(){
		for(int i=1;i<=root;i++) cnt[len[i]]++;
		for(int i=1;str[i];i++) cnt[i]+=cnt[i-1];
		for(int i=root;i>=1;i--) topu[cnt[len[i]]--]=i;
	}
	void build(){
		scanf("%s",str);root=last=1;
		for(int i=0;str[i];i++){
			add(str[i]-'a');
		}
	}
	void LCS(){
		scanf("%s",str);int now=1,maxlen=0,ans=0;
		int dlen=strlen(str);
		for(int i=0;i<dlen;i++){
			int id=str[i]-'a';
			if(son[now][id]){
				maxlen++;now=son[now][id];
			}else{
				for(;now&&!son[now][id];now=fa[now])
				if(!now){
					now=1;maxlen=0;
				}else{
					maxlen=len[now];
					now=son[now][id];
				}
			}
			ans=max(maxlen,ans);
		}
		printf("%d\n",ans);
	}
	void LCSwork(){
		build();
		scanf("%d",&n);
		for(int i=1;i<=n;i++) LCS();
		return;
	}
	};
}

namespace DStruct{
    struct Segment_tree{
    	int st[M],n,sum,maxf,minf;
    	struct segtree{
    		int l,r,maxv,minv,addv,setv,sumv;
    	}tr[M<<1];
    	void init(){memset(st,0,sizeof(st));sum=0;minf=inf,maxf=0;n=0;}
    	int ls(int a){return a<<1;}
    	int rs(int a){return a<<1|1;}
    	int getmid(int a,int b){return (a+b)>>1;}
    	void maintain(int o){
    		int L=ls(o),R=rs(o);
    		tr[o].sumv=tr[o].minv=tr[o].maxv=0;
    		if(tr[o].setv>=0){
    			tr[o].sumv=tr[o].setv*(tr[o].r-tr[o].l+1);
    			tr[o].minv=tr[o].maxv=tr[o].setv;
    		}
			else if(tr[o].r>tr[o].l){
    			tr[o].sumv=tr[L].sumv+tr[R].sumv;
    			tr[o].minv=min(tr[L].minv,tr[R].minv);
    			tr[o].maxv=max(tr[L].maxv,tr[R].maxv);
    		}
    		tr[o].minv+=tr[o].addv;tr[o].maxv+=tr[o].addv;tr[o].sumv+=tr[o].addv*(tr[o].r-tr[o].l+1);
    	}
    	void pushdown(int o){
    		int L=ls(o),R=rs(o);
    		if(tr[o].setv>=0){
    			tr[L].setv=tr[R].setv=tr[o].setv;
    			tr[L].addv=tr[R].addv=0;
    			tr[o].setv=-1;
    		}
    		if(tr[o].addv>0){
    			tr[L].addv+=tr[o].addv;
    			tr[R].addv+=tr[o].addv;
    			tr[o].addv=0;
    		}
    	}
    	void build(int o,int l,int r){
    		tr[o].l=l,tr[o].r=r;
    		tr[o].addv=0;tr[o].setv=-1;
    		if(l==r){
    			tr[o].maxv=tr[o].minv=tr[o].sumv=st[l];return;
    		}
    		int mid=getmid(l,r),L=ls(o),R=rs(o);
    		build(L,l,mid);
    		build(R,mid+1,r);
    		tr[o].minv=min(tr[L].minv,tr[R].minv);
    		tr[o].maxv=max(tr[L].maxv,tr[R].maxv);
    		tr[o].sumv=tr[L].sumv+tr[R].sumv;
    	}
		void upset_pos(int o,int pos,int val){
    		if(tr[o].l==tr[o].r){
    			tr[o].minv=val;tr[o].maxv=val;tr[o].sumv=val;return;
    		}
    		int mid=getmid(tr[o].l,tr[o].r),L=ls(o),R=rs(o);
    		if(pos<=mid) upset_pos(L,pos,val);
    		else upset_pos(R,pos,val);
    		tr[o].sumv=tr[L].sumv+tr[R].sumv;
    		tr[o].maxv=max(tr[L].maxv,tr[R].maxv);
    		tr[o].minv=min(tr[L].minv,tr[R].minv);
    	}//单点更新 
    	void updata_area(int o,int l,int r,int val){
    		 if(l<=tr[o].l&&tr[o].r<=r){
    		 	tr[o].addv+=val;
    		 }else{
    		 	pushdown(o);
    		 	int mid=getmid(tr[o].l,tr[o].r);
    		 	if(l<=mid) updata_area(o<<1,l,r,val);else maintain(o<<1);
    		 	if(r>mid) updata_area(o<<1|1,l,r,val);else maintain(o<<1|1);
    		 }
    		 maintain(o);
    	}
    	void query_pos(int o,int pos){
    		if(tr[o].l==tr[o].r){
    			maxf=max(maxf,tr[o].maxv);
    			minf=min(minf,tr[o].minv);
    			sum+=tr[o].sumv;return;
    		}
    		int mid=getmid(tr[o].l,tr[o].r);
    		if(pos<=mid) query_pos(o<<1,pos);
    		else query_pos(o<<1|1,pos);
    	}
    	void query_area(int o,int l,int r,int add){
    		if(tr[o].setv>=0){
    			sum+=(add+tr[o].setv+tr[o].addv)*(min(tr[o].r,r)-max(tr[o].l,l)+1);
    			minf=min(minf,tr[o].setv+tr[o].addv+add);
    			maxf=max(maxf,tr[o].setv+tr[o].addv+add);
    		}else if(l<=tr[o].l&&tr[o].r<=r){
    			sum+=tr[o].sumv+add*(tr[o].r-tr[o].l+1);
    			minf=min(minf,tr[o].minv+add);
    			maxf=max(maxf,tr[o].maxv+add);
    		}else{
    			int mid=getmid(tr[o].l,tr[o].r);
    			if(l<=mid) query_area(o<<1,l,r,add+tr[o].addv);
    			if(r>mid) query_area(o<<1|1,l,r,add+tr[o].addv);
			}
    	}
    	void updata_pos(int o,int pos,int val){
    		if(tr[o].l==tr[o].r){
    			tr[o].sumv+=val;tr[o].maxv+=val;
    			tr[o].minv+=val;return;
    		}
    			int mid=getmid(tr[o].l,tr[o].r);
    			if(pos<=mid) updata_pos(o<<1,pos,val);
    			else updata_pos(o<<1|1,pos,val);
    		tr[o].sumv=tr[o<<1].sumv+tr[o<<1|1].sumv;
    		tr[o].minv=min(tr[o<<1].minv,tr[o<<1|1].minv);
    		tr[o].maxv=max(tr[o<<1].maxv,tr[o<<1|1].maxv);
    	}
    	void upset_area(int o,int l,int r,int val){
    		if(l<=tr[o].l&&tr[o].r<=r){
    			tr[o].setv=val;
    			tr[o].addv=0;
    		}else{
    			pushdown(o);
    			int mid=getmid(tr[o].l,tr[o].r);
    			if(l<=mid) upset_area(o<<1,l,r,val);else maintain(o<<1);
    			if(r>mid) upset_area(o<<1|1,l,r,val);else maintain(o<<1|1);
    		}
    		maintain(o);
    	}
    };
    struct BITtree{
    	int btr[M],n;
    	void init(){memset(btr,0,sizeof(btr));}
    	int lowbit(int a){return a&(-a);}
    	void add(int a,int val){
    		for(;a<=n;a+=lowbit(a)) btr[a]+=val;
    	}
    	int getsum(int a){
    		int tot=0;
    		for(;a;a-=lowbit(a)) tot+=btr[a];
    		return tot;
    	}
    };
    struct Splaytree{
	int tot,f[M],son[M][2],val[M],root,size[M][2];
	int abbs(int a){return a<0?-a:a;}
	void add(int &a,int fa,int v){
		a=++tot;
		f[tot]=fa;val[tot]=v;
		son[tot][0]=son[tot][1]=0;
	}
	void revolve(int a,bool kind){
		int now=f[a];
		size[now][kind]=size[a][kind^1];
        size[a][kind^1]+=size[now][kind^1]+1;
		son[now][kind]=son[a][kind^1];
		f[son[now][kind]]=now;
        int k=son[f[now]][1]==now;
		if(f[now]) son[f[now]][k]=a;
		f[a]=f[now];f[now]=a;son[a][kind^1]=now;
	}
	void extend(int a,int to){
		while(f[a]!=to){
			int k=son[f[a]][1]==a;
			if(f[f[a]]==to) revolve(a,k);
			else {
				int b=f[a],kw=son[f[b]][1]==b;
				if(son[b][k^1]==a) revolve(a,k);
				else revolve(b,k);
				revolve(a,kw);
			}
		}
		if(!to)root=a;
	}
	bool insert(int v){
		int now=root;
		while(son[now][val[now]<v]){
			size[now][val[now]<v]++;
			if(val[now]==v){
			   extend(now,0);
			   return 0;
			}
			now=son[now][val[now]<v];
		}
		size[now][val[now]<v]++;
		add(son[now][val[now]<v],now,v);
		extend(son[now][val[now]<v],0);
		return 1;
	}
	int getmin(int a){
		int tt=son[a][0];
		if(!tt) return inf;
		for(;son[tt][1];tt=son[tt][1]);
		return abbs(val[tt]-val[a]);
	}
	int getmax(int a){
	    int tt=son[a][1];
	    if(!tt) return inf;
	    for(;son[tt][0];tt=son[tt][0]);
	    return abbs(val[tt]-val[a]);
	}
	int find(int a,int k){
       if(size[a][0]+1==k) return val[a];
       if(size[a][0]>=k) find(son[a][0],k);
       else find(son[a][1],k-size[a][0]-1);
    }
    };
    //树链剖分 
    struct TCD{
	int n,m,root,head[M],cnt;
	struct ss{
	    int to,last;
	}g[M<<1];
	void add(int a,int b){
		g[++cnt]=(ss){b,head[a]};head[a]=cnt;
		g[++cnt]=(ss){a,head[b]};head[b]=cnt;
	}//前向星链式存图 
	int pos[M],tot;
	struct pot{
		int son,top,fa,size,num,dep,val;
	}p[M];
	void dfs1(int a,int fa,int dp){
		p[a].fa=fa,p[a].dep=dp,p[a].size=1;
		for(int i=head[a];i;i=g[i].last){
		if(g[i].to==fa) continue;
		dfs1(g[i].to,a,dp+1);
		p[a].size+=p[g[i].to].size;
		if(!p[a].son||p[p[a].son].size<p[g[i].to].size)
		p[a].son=g[i].to;
		}
	}//找子树大小，重儿子，深度，父亲； 
	void dfs2(int a,int b){
		p[a].num=++tot;pos[p[a].num]=a;p[a].top=b;
		if(!p[a].son) return;
		dfs2(p[a].son,b);
		for(int i=head[a];i;i=g[i].last){
		if(g[i].to==p[a].fa||g[i].to==p[a].son) continue;
		dfs2(g[i].to,g[i].to);
		}
	}//重儿子遍历序 
	struct segment_tree{
	int l,r,sum,lazy;
	}tr[M<<1];
	#define L(x) (x<<1)
	#define R(x) (x<<1|1)
	#define M(x) (tr[x].l+tr[x].r>>1)
	void build(int o,int l,int r){
	tr[o].l=l;tr[o].r=r;tr[o].lazy=0;
	if(l==r){
		tr[o].sum=p[pos[l]].val;return;
	}
	int mid=M(o);
	build(L(o),l,mid);
	build(R(o),mid+1,r);
	tr[o].sum=tr[L(o)].sum+tr[R(o)].sum;
	}//初始化建树 
	void pushdown(int o){ 
	if(!tr[o].lazy) return;
	int l=L(o),r=R(o);
	tr[l].sum+=(tr[l].r-tr[l].l+1)*tr[o].lazy;
	tr[r].sum+=(tr[r].r-tr[r].l+1)*tr[o].lazy;
	tr[l].lazy+=tr[o].lazy;
	tr[r].lazy+=tr[o].lazy;
	tr[o].lazy=0;
	}//lazy下传 
	void updata(int o,int l,int r,int val){
	if(l<=tr[o].l&&tr[o].r<=r){
		tr[o].sum+=(tr[o].r-tr[o].l+1)*val;
		tr[o].lazy+=val;
		return;
	}
	pushdown(o);
	int mid=M(o);
	if(l<=mid) updata(L(o),l,r,val);
	if(r>mid) updata(R(o),l,r,val);
	tr[o].sum=tr[L(o)].sum+tr[R(o)].sum;
	}//区间更改 
	int query(int o,int l,int r){
	if(l<=tr[o].l&&tr[o].r<=r){
		return tr[o].sum;
	}
	pushdown(o);
	int mid=M(o),tt=0;
	if(l<=mid) tt+=query(L(o),l,r);
	if(r>mid) tt+=query(R(o),l,r);
	return tt;
	}//线段树区间查询 
	void add1(int a,int b,int val){
	for(;p[a].top!=p[b].top;a=p[p[a].top].fa){
		if(p[p[a].top].dep<p[p[b].top].dep) change(a,b);
		updata(1,p[p[a].top].num,p[a].num,val);
	}
	if(p[a].dep>p[b].dep) change(a,b);
	updata(1,p[a].num,p[b].num,val);
	}//链更改 
	void add2(int a,int val){
	updata(1,p[a].num,p[a].num+p[a].size-1,val);
	}//子树更改 
	int ask1(int a,int b){
	int now=0;
	for(;p[a].top!=p[b].top;a=p[p[a].top].fa){
		if(p[p[a].top].dep<p[p[b].top].dep) change(a,b);
		now+=query(1,p[p[a].top].num,p[a].num);
	}
	if(p[a].dep>p[b].dep) change(a,b);
	now+=query(1,p[a].num,p[b].num);
	return now;
	}//一条链之和询问 
	int ask2(int a){
	int now=query(1,p[a].num,p[a].num+p[a].size-1);
	return now;
	}//子树之和询问    	
    };
}
#endif
