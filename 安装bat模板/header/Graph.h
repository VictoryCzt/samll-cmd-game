#include<cstdio>
using namespace std;

namespace Graph{
template <class T>
	T min(T a,T b){return a<b?a:b;}
template <class T>
struct graph{
	private:
		int *had,*lst,*t;
		#ifdef OTHER
		T *v;
		#endif
		#ifdef FROM
		int *frm;
		#endif
		#ifdef COST
		T *cst;
		#endif
		int sze,newsze;
		int type;
	public:
		graph(int s=2){
			type=0;sze=0;newsze=s;
			had= new int[s];
			lst= new int[s];
			t  = new int[s];
			#ifdef OTHER
			v   = new T[s];
			#endif
			#ifdef FROM
			frm= new int[s];
			#endif
			#ifdef COST
			cst= new T[s];
			#endif
		}
		void premaxflow(){
			sze=1;type=1;
		}
		void premincost(){
			sze=1;type=2;
		}
		bool empty(){
			if(type!=0){
				return sze<=1;
			}
			return sze==0;
		}
		int size(){return sze;}
		void require(){
			if(sze<newsze-1) return;
			int *t1=had,*t2=lst,*t3=t;
			newsze=newsze<<1|1;
			newsze+=10;
			had= new int[newsze];
			lst= new int[newsze];
			t  = new int[newsze];
			#ifdef OTHER
			T *t4=v;
			v   = new T[newsze];
			#endif
			#ifdef FROM
			int *t5=from;
			frm= new int[newsze];
			#endif
			#ifdef COST
			T *t6=cost;
			cst= new T[newsze];
			#endif
			for(int i=1;i<=sze;++i){
				had[i]=t1[i];
				lst[i]=t2[i];
				t[i]=t3[i];
			#ifdef OTHER
				v[i]=t4[i];
			#endif
			#ifdef FROM
				frm[i]=t5[i];
			#endif
			#ifdef COST
				cst[i]=t6[i];
			#endif
			}
			delete [] t1;
			delete [] t2;
			delete [] t3;
			#ifdef OTHER
			delete [] t4;
			#endif
			#ifdef FROM
			delete [] t5;
			#endif
			#ifdef COST
			delete [] t6;
			#endif
		}
		void addedge(int a,int b,T c=0,T d=0){
			require();
			#ifdef OTHER
			v[sze+1]=c;
			#endif
			#ifdef FROM
			frm[sze+1]=b;
			#endif
			#ifdef COST
			cst[sze+1]=d;
			#endif
			lst[++sze]=had[a];t[sze]=b;had[a]=sze;
		}
		void twoedge(int a,int b,T c=0,T d=0){
			addedge(a,b,c,d);
			if(type==1) c=0;
			if(type==2) c=0,d=-d;
			addedge(b,a,c,d);
		}
		int head(int a){
			a=min(a,sze);
			return had[a];
		}
		int last(int a){
			a=min(a,sze);
			return lst[a];
		}
		int to(int a){
			a=min(a,sze);
			return t[a];
		}
		#ifdef OTHER
		T &val(int a){
			a=min(a,sze);
			return v[a];
		}
		#endif
		#ifdef FROM
		int from(int a){
			a=min(a,sze);
			return frm[a];
		}
		#endif
		#ifdef COST
		T &cost(int a){
			a=min(a,sze);
			return cst[a];
		}
		#endif
};
}
using namespace Graph;
